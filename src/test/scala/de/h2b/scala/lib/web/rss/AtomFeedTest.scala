/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.web.rss

import java.io.File
import java.net.URL
import java.text.SimpleDateFormat
import java.util.Locale

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.scala.lib.io._

@RunWith(classOf[JUnitRunner])
class AtomFeedTest extends FunSuite {

  val testAtomFile = getResource(getClass, "test.atom.xml")

  val dateFormatA = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.GERMANY)
  val dateFormatB = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", java.util.Locale.ENGLISH)

  val atomEntry1 = AtomEntry("Entry 1", Some("The first entry."), Some("This is the first entry."), Some(new URL("http://example.org")), Some("8401"), Some(dateFormatA.parse("19.01.2017 11:00:00")))
  val atomEntry2 = AtomEntry("Entry 2", Some("The second entry."), Some("This is the second entry."), Some(new URL("http://example.org")), Some("8402"), Some(dateFormatA.parse("19.01.2017 12:00:00")))
  val atomFeed = AtomFeed("Atom Test Feed", Some("h2b"), Some("84"), Some(dateFormatA.parse("19.01.2017 00:00:00")), Seq(atomEntry1, atomEntry2))

  test("date format") { //internal test
    assertResult("Thu Jan 19 01:00:00 CET 2017")(dateFormatA.parse("19.01.2017 01:00:00").toString)
    assertResult("Thu Jan 19 01:00:00 CET 2017")(dateFormatB.parse("Thu Jan 19 01:00:00 CET 2017").toString)
  }

  test("load") {
    assertResult(atomFeed)(AtomFeed.load(testAtomFile, dateFormatB))
  }

  test("save") {
    val file = File.createTempFile(getClass.getSimpleName, null)
    file.deleteOnExit()
    atomFeed.save(file)
    assertResult(atomFeed)(AtomFeed.load(testAtomFile, dateFormatB))
  }

}
