/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.web.rss

import java.io.File
import java.net.URL
import java.text.SimpleDateFormat
import java.util.Locale

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.scala.lib.io._

@RunWith(classOf[JUnitRunner])
class RssChannelTest extends FunSuite {

  val testRssFile = getResource(getClass, "test.rss.xml")

  val dateFormatA = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.GERMANY)
  val dateFormatB = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", java.util.Locale.ENGLISH)

  val rssItem1 = RssItem("Item 1", Some("The first item."), Some(new URL("http://example.org")), Some("h2b"), Some("4201"), Some(dateFormatA.parse("19.01.2017 01:00:00")))
  val rssItem2 = RssItem("Item 2", Some("The second item."), Some(new URL("http://example.org")), Some("h2b"), Some("4202"), Some(dateFormatA.parse("19.01.2017 02:00:00")))
  val rssChannel = RssChannel("RSS Test Channel", Some("An rss test channel."), Some(new URL("http://example.org")), Some(dateFormatA.parse("19.01.2017 00:00:00")), Seq(rssItem1, rssItem2))

  test("date format") { //internal test
    assertResult("Thu Jan 19 01:00:00 CET 2017")(dateFormatA.parse("19.01.2017 01:00:00").toString)
    assertResult("Thu Jan 19 01:00:00 CET 2017")(dateFormatB.parse("Thu Jan 19 01:00:00 CET 2017").toString)
  }

  test("load") {
    assertResult(rssChannel)(RssChannel.load(testRssFile, dateFormatB))
  }

  test("save") {
    val file = File.createTempFile(getClass.getSimpleName, null)
//    file.deleteOnExit()
    rssChannel.save(file)
    assertResult(rssChannel)(RssChannel.load(file, dateFormatB))
  }

}
