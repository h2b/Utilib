/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.web.rss

import java.util.Date

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class FeedTest extends FunSuite {

  val now = java.lang.System.currentTimeMillis()
  val delta = 3600L*1000 //1 hour

  val item1 = RssItem("Item 1", pubDate=Some(new Date(now)))
  val item2 = RssItem("Item 2", pubDate=Some(new Date(now+delta)))
  val item3 = RssItem("Item 3", pubDate=Some(new Date(now+2*delta)))

  val feed0 = RssChannel("Feed")
  val feed1 = RssChannel("Feed", items=Seq(item1))
  val feed2 = RssChannel("Feed", items=Seq(item1, item2))
  val feed3 = RssChannel("Feed", items=Seq(item1, item2, item3))
  val feed3r = RssChannel("Feed", items=Seq(item3, item2, item1))
  val feed3u = RssChannel("Feed", items=Seq(item3, item1, item2))

  test("with stamp") {
    val stamp = new Date()
    val probe = feed0.withStamp(stamp)
    assertResult(stamp)(probe.pubDate.get)
  }

  test("with items replaced") {
    assertResult(feed1)(feed3.withItems.replaced(Seq(item1)))
  }

  test("with items appended") {
    assertResult(feed3)(feed1.withItems.appended(Seq(item2, item3)))
  }

  test("with items prepended") {
    assertResult(feed3r)(feed1.withItems.prepended(Seq(item3, item2)))
  }

  test("with items removed") {
    assertResult(feed1)(feed3.withItems.removed(Seq(item2, item3)))
  }

  test("with items removed all") {
    assertResult(feed0)(feed3.withItems.removedAll())
  }

  test("with items reversed") {
   assertResult(feed3r)(feed3.withItems.reversed())
  }

  test("with items sorted") {
   assertResult(feed3)(feed3u.withItems.sortedByStampAscending())
   assertResult(feed3r)(feed3u.withItems.sortedByStampDescending())
   assert(!(feed3==feed3u.withItems.sortedByStampDescending()))
   assert(!(feed3r==feed3u.withItems.sortedByStampAscending()))
  }

}
