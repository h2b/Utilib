/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.web.rss

import java.net.URL
import java.text.SimpleDateFormat
import java.util.Locale

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.scala.lib.util._

@RunWith(classOf[JUnitRunner])
class RssItemTest extends FunSuite {

  val dateFormatA = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss", Locale.GERMANY)
  val dateFormatB = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy", java.util.Locale.ENGLISH)

  val rssItem1 = RssItem("Item 1", Some("The first item."), Some(new URL("http://example.org")), Some("h2b"), Some("4201"), Some(dateFormatA.parse("19.01.2017 01:00:00")))
  val rssItem1a = RssItem("Item 1", Some("The first item."), Some(new URL("http://example.org")), Some("h2b"), Some("4201"), Some(dateFormatA.parse("19.01.2017 01:00:00")))
  val rssItem2 = RssItem("Item 2", Some("The second item."), Some(new URL("http://example.org")), Some("h2b"), Some("4202"), Some(dateFormatA.parse("19.01.2017 02:00:00")))
  val rssItem3 = RssItem("Item 3", Some("The third item."), None, None, Some("4203"), None)

  val elem1 =
		<item>
			<title>Item 1</title>
			<description>The first item.</description>
			<link>http://example.org</link>
			<author>h2b</author>
			<guid>4201</guid>
			<pubDate>Thu Jan 19 01:00:00 CET 2017</pubDate>
		</item>

  val elem3 =
		<item>
			<title>Item 3</title>
			<description>The third item.</description>
			<guid>4203</guid>
		</item>

  test("date format") { //internal test
    assertResult("Thu Jan 19 01:00:00 CET 2017")(dateFormatA.parse("19.01.2017 01:00:00").toString)
    assertResult("Thu Jan 19 01:00:00 CET 2017")(dateFormatB.parse("Thu Jan 19 01:00:00 CET 2017").toString)
  }

  test("equality") {
    assert(rssItem1==rssItem1a)
    assert(rssItem1!=rssItem2)
    assert(rssItem1!=rssItem3)
  }

  test("to elem") {
    assert(rssItem1.toElem().toString() ~= elem1.toString())
    assert(rssItem3.toElem().toString() ~= elem3.toString())
  }

  test("from elem") {
    assertResult(rssItem1)(RssItem.fromNode(elem1, dateFormatB))
    assertResult(rssItem3)(RssItem.fromNode(elem3, dateFormatB))
  }

}
