/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.io

import java.io.IOException

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.scala.lib.io._

@RunWith(classOf[JUnitRunner])
class PackageObjectTest extends FunSuite {

  test("get resource") {
    getResource(getClass, "LoremIpsum2.txt")
  }

  test("resource") {
    resource(getClass, "LoremIpsum2.txt")
  }

  test("is copy of") {
    val f1a = getResource(getClass, "LoremIpsum1a.txt")
    val f1b = getResource(getClass, "LoremIpsum1b.txt")
    val f2 = getResource(getClass, "LoremIpsum2.txt")
    assert(f1a.isCopyOf(f1b))
    assert(!f1a.isCopyOf(f2))
  }

}
