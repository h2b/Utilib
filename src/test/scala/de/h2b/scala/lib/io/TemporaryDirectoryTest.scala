/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.io

import java.io.IOException

import org.junit.runner.RunWith
import org.scalatest.{ BeforeAndAfter, FunSuite }
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class TemporaryDirectoryTest extends FunSuite with BeforeAndAfter {

  var tmpDir: TemporaryDirectory = _

  before {
	  tmpDir = TemporaryDirectory()
  }

  after {
    tmpDir.destroy()
  }

  test("add file") {
    testForResource("TemporaryDirectoryTestResources/testfile.txt")
  }

  private def testForResource (name: String) = {
    val source = getResource(classOf[TemporaryDirectoryTest], name)
    val target = tmpDir.add(source)
    assertResult(source.getName)(target.getName)
    assert(target.isCopyOf(source))
  }

  test("add tree") {
    testForResource("TemporaryDirectoryTestResources/testdir")
  }

  test("create unnamed dir") {
    val name = ""
    testDir(name)
  }

  private def testDir (name: String) = {
    val target = tmpDir.createDir(name)
    assert(target.isDirectory())
    assert(target.list().isEmpty)
    target
  }

  test("create named dir") {
    val name = "testdir/sub1/sub2"
    val dir = testDir(name)
    assertResult("sub2")(dir.getName)
    assertResult("sub1")(dir.getParentFile.getName)
    assertResult("testdir")(dir.getParentFile.getParentFile.getName)
  }

  test("create unnamed file") {
    val name = ""
    testFile(name)
  }

  private def testFile (name: String) = {
    val target = tmpDir.createFile(name)
    assert(target.isFile())
    assertResult(0L)(target.length())
    target
  }

  test("create named file") {
    val name = "testdir/subdir/testfile"
    val file = testFile(name)
    assertResult("testfile")(file.getName)
    assertResult("subdir")(file.getParentFile.getName)
    assertResult("testdir")(file.getParentFile.getParentFile.getName)
  }

}