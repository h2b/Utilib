/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.stat

import org.junit.runner.RunWith
import org.scalatest.{ BeforeAndAfter, FunSuite }
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ShuffleTest extends FunSuite with BeforeAndAfter {

  private val elements = 1 to 29
  private val origin = Array(elements: _*)
  private var shuffled: Array[Int] = _

  println(origin.mkString(","))

  before {
    shuffled = Array(origin.toSeq: _*)
    Shuffle(shuffled)
    println(shuffled.mkString(","))
  }

  test("shuffled array has same length") {
	  assertResult(origin.length)(shuffled.length)
  }

  test("shuffled is not same") {
    assert(origin!=shuffled)
  }

  test("shuffled contains all elements") {
	  for (el <- elements) assert(shuffled.contains(el))
  }

}