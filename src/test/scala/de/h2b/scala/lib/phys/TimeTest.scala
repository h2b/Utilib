/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.phys

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
 * @author h2b
 */
@RunWith(classOf[JUnitRunner])
class TimeTest extends FunSuite {

  test("verify some conversions to string") {
    assertResult("0ms")(Time(0).toString);
    assertResult("15ms")(Time(15).toString);
    assertResult("25s")(Time(25000).toString);
    assertResult("1h")(Time(3600000).toString);
    assertResult("1d12h30m1ms")(Time(86400000+12*3600000+30*60000+1).toString);
  }

  test("intercept exception for negative milliseconds") {
    intercept[IllegalArgumentException](Time(-1))
  }

  test("verify some conversions from string") {
    assertResult((3*86400+25*60)*1000)(Time("3d25m").ms)
    assertResult((5*60+12)*1000+125)(Time("5m12s125ms").ms)
    assertResult((4*86400+23*3600+5*60+12)*1000+125)(Time("4d23h5m12s125ms").ms)
    assertResult(0)(Time("").ms)
  }

  test("intercept exception for malformed time string") {
    intercept[IllegalArgumentException](Time("x"))
  }

  test("equals") {
    val t1 = Time(100)
    val t2 = Time(200)
    assert(t1==t1)
    assert(!(t1==t2))
  }

  test("unapply") {
    val s = 1000
    Time(s) match {
      case Time(ms) => assertResult(s)(ms)
      case _ => fail()
    }
  }

}
