/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util.config

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import org.scalatest.FunSuite

@RunWith(classOf[JUnitRunner])
class FileConfigTest extends FunSuite {

  private val filename = "TestFileConfig.txt"
  private val config = FileConfig(getClass, filename)

  test("config read") {
    assertResult(3)(config.size)
    assertResult(Some("Value A"))(config.get("de.h2b.scala.lib.util.config.KeyA"))
    assertResult(Some("Value B"))(config.get("de.h2b.scala.lib.util.config.KeyB"))
    assertResult(Some("Value C"))(config.get("de.h2b.scala.lib.util.config.KeyC"))
  }

}