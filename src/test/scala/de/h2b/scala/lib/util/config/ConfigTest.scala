/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util.config

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ConfigTest extends FunSuite {

  private val config = Config("one"->1, "two"->2, "three"->3)

  test("get") {
    assertResult(Some(1))(config.get("one"))
    assertResult(Some(2))(config.get("two"))
    assertResult(Some(3))(config.get("three"))
    assertResult(None)(config.get("four"))
  }

  test("iterator") {
    val s = config.iterator.toSet
    assert(isValidated(s, "one"->1, "two"->2, "three"->3))
  }

  private def isValidated (s: Set[(String, Int)], els: (String, Int)*): Boolean = {
    var result = s.size==els.size
    for (e <- els) result = result && s.contains(e)
    result
  }

  test("+") {
    val sum = config + ("four"->4)
    assert(isValidated(sum.toSet, "one"->1, "two"->2, "three"->3, "four"->4))
  }

  test("-") {
    val dif = config - "two"
    assert(isValidated(dif.toSet, "one"->1, "three"->3))
  }

  test("empty") {
    val e = config.empty
    assertResult(0)(e.size)
  }
  
  test("types") {
    type configType = Config[String,Int]
    assert(config.isInstanceOf[configType])
    assert(config.empty.isInstanceOf[configType])
    assert((config + ("four"->4)).isInstanceOf[configType])
    assert((config - "two").isInstanceOf[configType])
    assert((config map { (e: (String, Int)) => (e._1, e._2+1) }).isInstanceOf[configType])
  }

}
