/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util

import java.text.{ ParseException, SimpleDateFormat }
import java.util.GregorianCalendar

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class PackageObjectTest extends FunSuite {

  test("string similarity") {
    assert("abc" ~= "abc")
    assert("abc" ~= "a b c")
    assert("abc" ~= "abc\n")
    assert("abc" ~= "a\nb\tc ")
    assert(!("abc" ~= "abx"))
    assert(!("abc" ~= null))
  }

  test("toTime") {
    assertResult((3*86400+25*60)*1000)("3d25m".toTime.ms)
    assertResult((5*60+12)*1000+125)("5m12s125ms".toTime.ms)
    assertResult((4*86400+23*3600+5*60+12)*1000+125)("4d23h5m12s125ms".toTime.ms)
    assertResult(0)("".toTime.ms)
  }

  test("intercept exception for malformed time string") {
    intercept[IllegalArgumentException]("x".toTime)
  }

  test("toDate") {
    val date = new GregorianCalendar(2016, 7-1, 31).getTime
    assertResult(date)("2016-07-31".toDate(new SimpleDateFormat("yyyy-MM-dd")))
  }

  test("intercept exception for malformed date string") {
    intercept[ParseException]("x".toDate(new SimpleDateFormat("yyyy-MM-dd")))
  }
  
  test("linesWithoutSeparators") {
    val probe = "line1\nline2\nline3"
    val result = probe.linesWithoutSeparators.toSeq
    assertResult("line1")(result(0))
    assertResult("line2")(result(1))
    assertResult("line3")(result(2))
  }

}