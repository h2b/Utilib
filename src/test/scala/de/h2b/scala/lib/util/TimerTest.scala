/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
 * @author h2b
 */
@RunWith(classOf[JUnitRunner])
class TimerTest extends FunSuite {

  test("compare delay with system time") {
    val ms = 200
    val tolerance = ms/100
    val t1 = System.currentTimeMillis
    Timer.delay(ms)
    val t2 = System.currentTimeMillis
    val delta = t2-t1
    assert(ms-tolerance<=delta && delta<=ms+tolerance, "measured time out of tolerance")
  }

  test("check simple ms timer") {
    val ms = 100
    val tolerance = ms/100
    val t = Timer.milliTimer()
    Timer.delay(ms)
    val delta = t.stop()
    assert(ms-tolerance<=delta && delta<=ms+tolerance, "measured time out of tolerance")
  }

  test("check simple ns timer") {
    val ns = 100000000L
    val tolerance = ns/100
    val t = Timer.nanoTimer()
    Timer.delay(ns/1000000)
    val delta = t.stop()
    assert(ns-tolerance<=delta && delta<=ns+tolerance, "measured time out of tolerance")
  }

  test("check start/stop cycles") {
    val ms = Vector(100, 200, 100)
    val sum = ms.sum
    val tolerance = sum/100
    val t = Timer()
    for (msi<-ms) {
      t.start()
      Timer.delay(msi)
      t.stop()
      Timer.delay(25) //add some extra time out of the cycles
    }
    assert(sum-tolerance<=t.reading && t.reading<=sum+tolerance, "measured time out of tolerance")
  }

  test("check lap function") {
    val ms = 100
    val tolerance = ms/20
    val t = Timer()
    for (i <- 1 to 5) {
      Timer.delay(ms)
      val l = t.lap
    	assert(i*ms-tolerance<=l && l<=i*ms+tolerance, "measured time out of tolerance")
    }
  }

  test("check reset function") {
	  val ms = 100
    val tolerance = ms/100
    val t = Timer()
    Timer.delay(ms)
    t.reset()
    val delta = t.stop()
    assert(-tolerance<=delta && delta<=tolerance, "measured time out of tolerance")
  }

}