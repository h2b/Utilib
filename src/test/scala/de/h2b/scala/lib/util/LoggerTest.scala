/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util

import scala.collection.mutable.Set

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
 * @author h2b
 */
@RunWith(classOf[JUnitRunner])
class LoggerTest extends FunSuite {

  Logger.info("test")

  val defaultLevel = Level.Info

  test("global logger has expected default level") {
    assert(Logger.isEnabled(defaultLevel))
    assert(!Logger.isEnabled(Level.Debug))
  }

  test("logger can be set to other level") {
    val logger = Logger("test") at Level.Debug
    assert(logger.isEnabled(Level.Debug))
  }

  test("toString yields expected result") {
	  assertResult("de.h2b.scala.lib.util.ConsoleLogger(de.h2b.scala.lib.util.LoggerTest)")(Logger(getClass).toString)
  }

  test("call by name") {
    val logger = Logger("test") at Level.Warn
    val done = Set.empty[String]
    def msg (text: String) = { done += text; s"OK (just testing $text)" }
    logger.trace(msg("trace"))
    logger.debug(msg("debug"))
    logger.config(msg("config"))
    logger.info(msg("info"))
    logger.warn(msg("warn"))
    logger.error(msg("error"))
    logger.fatal(msg("fatal"))
    assert(Set("warn", "error", "fatal") == done)
  }

}
