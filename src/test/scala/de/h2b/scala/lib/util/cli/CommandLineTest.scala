/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util.cli

import org.junit.runner.RunWith
import org.scalatest.{ BeforeAndAfter, FunSuite }
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class CommandLineTest extends FunSuite with BeforeAndAfter {

  import de.h2b.scala.lib.util.cli.Converter._

  var flagParam: FlagParameter = _
  var valueParam: ValueParameter[Int] = _
  var listParam: ListParameter[Int] = _
  var mainParam: MainParameter = _

  var parameters: Set[Parameter[_]] = _
  var cmd: CommandLine = _

  before {
	  flagParam = FlagParameter(Set("-a", "--all"), "flags all")
	  valueParam = ValueParameter(Set("-i"), "an int", default=Some(42))
	  listParam = ListParameter[Int](Set("-m"), "3 values", arity=3)
	  mainParam = MainParameter("source target", 2)
	  parameters = Set(flagParam, valueParam, listParam, mainParam)
	  cmd = CommandLine(parameters)
  }

  test("no argument") {
	  cmd.parse()
	  assertResult(None)(flagParam.value)
	  assertResult(Some(42))(valueParam.value)
 	  assertResult(None)(listParam.values)
	  assertResult(None)(mainParam.values)
  }

  test("single argument") {
	  cmd.parse("-i", "123")
	  assertResult(None)(flagParam.value)
	  assertResult(Some(123))(valueParam.value)
 	  assertResult(None)(listParam.values)
	  assertResult(None)(mainParam.values)
  }

  test("multiple arguments") {
	  cmd.parse("-a -i 123 -m 1 2 3".split(' '))
	  assertResult(Some(true))(flagParam.value)
	  assertResult(Some(123))(valueParam.value)
 	  assertResult(Some(Seq(1, 2, 3)))(listParam.values)
	  assertResult(None)(mainParam.values)
  }

  test("main arguments") {
 	  cmd.parse("main1", "main2")
	  assertResult(None)(flagParam.value)
	  assertResult(Some(42))(valueParam.value)
 	  assertResult(None)(listParam.values)
	  assertResult(Some(Seq("main1", "main2")))(mainParam.values)
  }

  test("main arguments mixed with options") {
	  cmd.parse("main1 -a -i 123 -m 1 2 3 main2".split(' '))
	  assertResult(Some(true))(flagParam.value)
	  assertResult(Some(123))(valueParam.value)
 	  assertResult(Some(Seq(1, 2, 3)))(listParam.values)
	  assertResult(Some(Seq("main1", "main2")))(mainParam.values)
  }

  test("main arguments after flag") ({
    val extracted = "-a main1 main2".split(' ')
    cmd.parse(extracted)
    assertResult(Some(true))(flagParam.value)
    assertResult(Some(42))(valueParam.value)
    assertResult(None)(listParam.values)
    assertResult(Some(Seq("main1", "main2")))(mainParam.values)
  })

  test("intercept missing required argument exception") {
    val r = ValueParameter[Int](Set("-r"), "a required int", required=true)
    val thrown = intercept[CommandLineException] {
      CommandLine(Set(r)).parse()
    }
    val expected = "missing parameters required: ValueParameter(Set(-r),a required int,true,None)"
    assertResult(expected)(thrown.getMessage)
  }

  test("intercept wrong option arity exception") {
    val thrown = intercept[ParameterException] {
    	cmd.parse("-m 1 2".split(' '))
    }
    val expected = "Parameter Set(-m): wrong number of arguments: 2"
    assertResult(expected)(thrown.getMessage)
  }

  test("don't throw exception when required main argument is given") {
    val m = MainParameter("arg", 1, required=true)
    CommandLine(Set(m)).parse("main")
	  assertResult(Some(Seq("main")))(m.values)
  }

  test("intercept missing required main argument exception") {
    val m = MainParameter("arg", 1, required=true)
    val thrown = intercept[CommandLineException] {
      CommandLine(Set(m)).parse()
    }
    val expected = "missing parameters required: MainParameter(arg,1,true,None)"
    assertResult(expected)(thrown.getMessage)
  }

  test("intercept wrong main-parameter arity exception") {
    val thrown = intercept[ParameterException] {
    	cmd.parse("main1")
    }
    val expected = "Parameter Set(--): wrong number of arguments: 1"
    assertResult(expected)(thrown.getMessage)
  }

  test("intercept wrong main-parameter arity exception mixed with options") {
    val thrown = intercept[ParameterException] {
    	cmd.parse("-a -i 123 -m 1 2 3 main2".split(' '))
    }
    val expected = "Parameter Set(--): wrong number of arguments: 1"
    assertResult(expected)(thrown.getMessage)
  }

  test("intercept conversion error exception") {
    val thrown = intercept[ParameterException] {
    	cmd.parse("-i", "NaN")
    }
    val expected = "Parameter Set(-i): conversion error"
    assertResult(expected)(thrown.getMessage)
  }

  test("help parameter") {
    val h = HelpParameter(Set("-h", "--help"))
    val r = ValueParameter[Int](Set("-r"), "a required int", required=true)
    CommandLine(parameters ++ Set(h, r)).parse("-h -a -i iii -m 1 2 main".split(' '))
	  assertResult(None)(flagParam.value)
	  assertResult(Some(42))(valueParam.value)
 	  assertResult(None)(listParam.values)
	  assertResult(None)(mainParam.values)
	  assertResult(Some(true))(h.value)
  }

  test("variable arity options") {
	  val v1 = ListParameter[Int](Set("-v1"), "min. 3 values", arity = -3)
	  val v2 = ListParameter[Int](Set("-v2"), "min. 2 values", arity = -2)
	  val c = CommandLine(parameters ++ Set(v1, v2))
	  c.parse("-a -v1 11 12 13 -i 123 -m 1 2 3 -v2 21 22 23 24".split(' '))
	  assertResult(Some(true))(flagParam.value)
	  assertResult(Some(123))(valueParam.value)
 	  assertResult(Some(Seq(1, 2, 3)))(listParam.values)
	  assertResult(None)(mainParam.values)
 	  assertResult(Some(Seq(11, 12, 13)))(v1.values)
 	  assertResult(Some(Seq(21, 22, 23, 24)))(v2.values)
  }

  test("variable arity main parameter") {
	  val m = MainParameter("m ...", -1)
	  val c = CommandLine(Set(flagParam, valueParam, listParam, m))
 	  c.parse("main1", "main2", "main3")
	  assertResult(None)(flagParam.value)
	  assertResult(Some(42))(valueParam.value)
 	  assertResult(None)(listParam.values)
	  assertResult(Some(Seq("main1", "main2", "main3")))(m.values)
  }

  test("variable arity main parameter mixed with options") {
	  val m = MainParameter("m ...", -2)
	  val v = ListParameter[Int](Set("-v"), "min. 2 values", arity = -2)
	  val c = CommandLine(Set(flagParam, valueParam, listParam, v, m))
	  c.parse("main1 -a -v 11 12 13 14 -i 123 -m 1 2 3 main2 main3".split(' '))
	  assertResult(Some(true))(flagParam.value)
	  assertResult(Some(123))(valueParam.value)
 	  assertResult(Some(Seq(1, 2, 3)))(listParam.values)
 	  assertResult(Some(Seq(11, 12, 13, 14)))(v.values)
	  assertResult(Some(Seq("main1", "main2", "main3")))(m.values)
  }

  test("command-line usage") {
    val r = ValueParameter[Int](Set("-r"), "a required int", required=true)
	  val c = CommandLine(Set(flagParam, valueParam, listParam, mainParam, r))
	  val expected =
	    """Usage:
        |  test options source target
        |where options are:
        |  -a, --all   flags all
        |  -i          an int
        |  -m          3 values
        |  -r        * a required int
        |Required arguments are marked with *.
        |This is a test.""".stripMargin
    val actual = c.usage("test", 9, "This is a test.")
    assertResult(expected)(actual)
  }

}
