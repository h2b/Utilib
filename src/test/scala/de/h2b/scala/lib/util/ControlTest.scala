/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

/**
 * @author h2b
 */
@RunWith(classOf[JUnitRunner])
class ControlTest extends FunSuite {

  test("using performs block and closes resource") {
    object testResource {
      var isClosed = false
      var done = false
      def close () = isClosed = true
      def doSomething () = done = true
    }
    Control.using(testResource) { resource =>
    	resource.doSomething()
    }
    assert(testResource.done)
    assert(testResource.isClosed)
  }

  class TestOp {
    val succesCount = 3
    var count = 0
    def run (): Int = {
      count += 1
      require(count==succesCount)
      count
    }
  }

  test("retry raises exception if count too low") {
    val op = new TestOp()
    intercept[IllegalArgumentException](Control.retry(1)(op.run()))
  }

  test("retry succeeds if count is high enough") {
    val op = new TestOp()
    val count = Control.retry(op.succesCount+5)(op.run())
    assertResult(op.succesCount)(count)
  }

}