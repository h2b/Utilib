/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util.config

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ArgsConfigTest extends FunSuite {

  private val args = Array("-x", "key1=val1", "/y", "key2=val2", "abc")
  private val config = ArgsConfig(args)

  test("config read") {
    assertResult(2)(config.size)
    assertResult(Some("val1"))(config.get("key1"))
    assertResult(Some("val2"))(config.get("key2"))
  }

  test("remaining") {
    val r = config.remaining
    assertResult(3)(r.size)
    assertResult(Array("-x", "/y", "abc"))(r)
  }

}
