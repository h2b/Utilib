/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.scala.lib.util.PerformanceMeter.Task

@RunWith(classOf[JUnitRunner])
class PerformanceMeterTest extends FunSuite {

  class TestTask extends Task {
	  var preCalls, postCalls, perfCalls = 0
	  override def prePerform () = preCalls += 1
	  override def postPerform () = postCalls += 1
	  def perform () = perfCalls += 1
  }

  test("task methods are called the right number of times") {
    val task = new TestTask()
    val repeats = 3
    PerformanceMeter.measurements(Seq(task), repeats)
    assertResult(repeats)(task.perfCalls)
    assertResult(repeats)(task.preCalls)
    assertResult(repeats)(task.postCalls)
  }

  test("task is in result map") {
    val task = new TestTask()
    val repeats = 1
    val result = PerformanceMeter.measurements(Seq(task), repeats)
    assert(result.contains(task))
  }

  test("empty task sequence yields empty result map") {
    val result = PerformanceMeter.measurements(Seq.empty[Task], 1)
    assert(result.isEmpty)
  }

  test("task methods are not called for zero repititions") {
    val task = new TestTask()
    val repeats = 0
    PerformanceMeter.measurements(Seq(task), repeats)
    assertResult(0)(task.perfCalls)
    assertResult(0)(task.preCalls)
    assertResult(0)(task.postCalls)
  }

  test("resulting measurement is zero for zero repititions") {
    val task = new TestTask()
    val repeats = 0
    val result = PerformanceMeter.measurements(Seq(task), repeats)
    assertResult(0)(task.postCalls)
    assertResult(0L)(result(task))
  }

}