/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util.cli

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class ParameterTest extends FunSuite {

  test("parameter usage") {
	  val p = FlagParameter(Set("-a", "--all"), "flags all")
    assertResult("-a, --all    flags all\n")(p.usage(10))
  }

  test("parameter usage with long description") {
	  val p = FlagParameter(Set("-t"),
	    """Line 1
	      |Line 2
	      |Line 3""".stripMargin)
    val expected =
	    """-t   Line 1
	      |     Line 2
	      |     Line 3
	      |""".stripMargin
    assertResult(expected)(p.usage())
  }

}
