/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util.config

import scala.collection.immutable.{ Iterable, Map, MapOps }
import scala.collection.mutable.{ Builder, Map => MutableMap }
import scala.collection.{ Factory, MapFactory, MapFactoryDefaults }

/**
 * An immutable type that provides configuration properties as a map of
 * key/value pairs.
 *
 * @note This variant of the source code is for Scala from version 2.13 on.
 *
 * @param <Key> the type of the keys
 * @param <Value> the type of the associated values
 * @author h2b
 */
trait Config [Key, +Value] extends Map[Key, Value] with 
    MapOps[Key, Value, Config, Config[Key, Value]] with
    MapFactoryDefaults[Key, Value, Config, Iterable] {

  /**the underlying map by which subclasses provide the key/value pairs*/
  protected val props: Map[Key, Value]

  def get (key: Key): Option[Value] = props.get(key)

  def iterator: Iterator[(Key, Value)] = props.iterator

  def updated [V >: Value] (key: Key, value: V): Config[Key, V] = {
    val builder = Config.newBuilder[Key, V]
    builder ++= this
    builder += ((key, value))
    builder.result()
  }

  def removed (key: Key): Config[Key, Value] = {
    val builder = Config.newBuilder[Key, Value]
    builder ++= this filter (key != _._1)
    builder.result()
  }
  
  override def mapFactory: MapFactory[Config] = new ConfigFactory()

}

object Config {

	/**
	 * @return an empty config
	 */
	def empty [Key, Value]: Config[Key, Value] = new Config[Key, Value] {
	  protected val props = Map.empty[Key, Value]
  }

  /**
   * A collection of type `Config` that contains given key/value bindings.
   *
   * @param props the key/value pairs that make up the config
   * @return a new config consisting of key/value pairs given by props
   */
  def apply [Key, Value] (props: (Key, Value)*): Config[Key, Value] = {
    val builder = newBuilder[Key, Value]
    for (prop <- props) builder += prop
    builder.result()
  }
  
  def from [Key, Value] (source: IterableOnce[(Key, Value)]): Config[Key, Value] =
    source match {
	    case config: Config[Key, Value] => config
      case _ => (newBuilder ++= source).result()
    }

  def newBuilder [Key, Value]: Builder[(Key, Value), Config[Key, Value]] = 
    new Builder[(Key, Value), Config[Key, Value]] {
	    private val elems = MutableMap.empty[Key, Value]
    	def addOne (elem: (Key, Value)): this.type = { elems += elem; this }
    	def clear (): Unit = elems.clear()
    	def result (): Config[Key, Value] = new Config[Key, Value] {
        protected val props: Map[Key, Value] = elems.toMap
    	}
    }
    
    import scala.language.implicitConversions
    
    implicit def toFactory [Key, Value] (self: this.type): 
        Factory[(Key, Value), Config[Key, Value]] = 
      new Factory[(Key, Value), Config[Key, Value]] {
        def fromSpecific (coll: IterableOnce[(Key, Value)]): Config[Key, Value] = 
          self.from(coll)
        def newBuilder: Builder[(Key, Value), Config[Key, Value]] = 
          self.newBuilder
      }
        
}

class ConfigFactory extends MapFactory[Config] {

  def from [Key, Value] (source: IterableOnce[(Key, Value)]): Config[Key, Value] =
    Config.from(source)

  def newBuilder [Key, Value]: Builder[(Key, Value), Config[Key, Value]] = 
    Config.newBuilder
    
  def empty [Key, Value]: Config[Key, Value] =
    Config.empty

}
