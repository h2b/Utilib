/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util

import scala.collection.convert.{ AsJavaExtensions, AsScalaExtensions }

/**
 * An equivalent to `scala.jdk.CollectionConverters`. To be used to import this
 * object from a single package over different Scala versions in order to maintain
 * source-code compatibilty.
 * @note This variant of the source code is for Scala from version 2.13 on.
 */
object JavaCollectionConverters extends AsJavaExtensions with AsScalaExtensions
