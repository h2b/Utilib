/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib

import java.io.{ File, IOException }
import java.nio.file.{ Path, Paths }

import scala.io.{ Codec, Source }
import scala.language.implicitConversions

package object io {

  implicit def string2File (s: String): File = new File(s)

  implicit def string2Path (s: String): Path = Paths.get(s)

  implicit def file2Path (f: File): Path = f.toPath()

  /**
   * Returns a resource file of specified name associated with a class.
   *
   * The resource is looked up under the modified package name of the given class
   * (i.e.,  with '/' substituted for '.'), unless the resource name begins with
   * a '/', in which case it is looked up under the resource name itself
   * (stripping the leading '/').
   *
   * @param c the class for which the resource shall be got
   * @param name the name of the desired resource
   * @return the resource as file
   * @throws java.io.IOException if no resource with this name is found
   */
  @throws(classOf[IOException])
  def getResource (c: Class[_], name: String): File = {
    val url = c.getResource(name)
    if (url==null) throw new IOException(s"resource not found: $name for class $c")
    new File(url.toURI())
  }

  /**
   * Returns a resource of specified name associated with a class.
   *
   * The resource is looked up under the modified package name of the given class
   * (i.e.,  with '/' substituted for '.'), unless the resource name begins with
   * a '/', in which case it is looked up under the resource name itself
   * (stripping the leading '/').
   *
   * @param c the class for which the resource shall be got
   * @param name the name of the desired resource
   * @param codec implicit character encoding
   * @return the resource as source
   * @throws java.io.IOException if no resource with this name is found
   */
  @throws(classOf[IOException])
  def resource (c: Class[_], name: String) (implicit codec: Codec): Source =
    Source.fromFile(getResource(c, name))(codec)

  implicit class FileOps (file: File) {

    /**
     * Tests if this file has the same content as another one.
     *
     * @param other the other file
     * @param codec implicit character encoding
     * @return this file has the same content as the other one
     * @throws java.io.IOException if an I/O exception occurred
     */
    @throws(classOf[IOException])
    def isCopyOf (other: File) (implicit codec: Codec): Boolean = {
      other != null && isSame(file, other, codec)
    }

    private def isSame (a: File, b: File, codec: Codec): Boolean =
      if (a.isDirectory()) isSameTree(a, b, codec)
      else a.length()==b.length() && hasSameContent(a, b, codec)

    private def isSameTree (a: java.io.File, b: java.io.File, codec: scala.io.Codec): Boolean = {
      val files1 = a.listFiles()
      val files2 = b.listFiles()
      var result = true
      for (f1 <- files1) {
        val f2 = files2 find { f: File => f.getName==f1.getName }
        if (f2.isEmpty) return false
        result &&= isSame(f1, f2.get, codec)
      }
      result
    }

    private def hasSameContent (a: File, b: File, codec: Codec) = {
      val s1 = Source.fromFile(a)(codec)
      val s2 = Source.fromFile(b)(codec)
      val result = s1.mkString==s2.mkString
      s1.close()
      s2.close()
      result
    }
  }

}