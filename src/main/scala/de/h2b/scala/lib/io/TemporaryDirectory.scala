/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.io

import java.io.{ File, IOException }
import java.nio.file.{ CopyOption, Files, Path }

class TemporaryDirectory (target: Option[Path]) {

  import TemporaryDirectory._

  private val path = (target match {
    case Some(dir) => Files.createTempDirectory(dir, RootPrefix)
    case None => Files.createTempDirectory(RootPrefix)
  }).toFile()

  private var files = Nil: List[File]

  private var _isInActiveState = true

  def isInActiveState = _isInActiveState

  /**
   * Adds a file or a directory to this temporary directory by copying it from
   * the source location. In case of a directory, the whole tree is copied.
   *
   * @param f the source location, must be a regular file or a directory
   * @param options
   * @return the target location
   * @throws java.io.IOException
   * @throws IllegalArgumentException if f is not a regular file or a directory
   * @throws IllegalStateException if this object is not in active state
   */
  @throws(classOf[IOException])
  def add (f: File, options: CopyOption*): File = {
    if (!isInActiveState) throw new IllegalStateException("not in active state")
    require(f.isFile || f.isDirectory, f.getAbsolutePath + " is neither a file nor a directory")
    copy(f, path, options)
  }

  private def copy (source: File, target: File, options: Seq[CopyOption]): File = {
    val result = Files.copy(source.toPath(), target.toPath().resolve(source.getName), options: _*).toFile()
    files ::= result
    if (source.isDirectory) {
    	for (f <- source.listFiles()) copy(f, result, options)
    }
    result
  }

  /**
   * Creates a new directory within this temporary directory.
   *
   * If the `dir` argument is empty, a unique directory is created.
   *
   * If the `dir` argument denotes a path with elements separated by any
   * characters defined by `Separators`, the whole path is created as necessary,
   * but the last element of the path must not exist before.
   *
   * @param dir
   * @return the target location
   * @throws IOException if the directory cannot be created
   */
  @throws(classOf[IOException])
  def createDir (dir: String): File = {
    if (!isInActiveState) throw new IllegalStateException("not in active state")
    if (dir.isEmpty()) return Files.createTempDirectory(path.toPath(), SubPrefix).toFile()
    val names = dir.split(Separators)
    val parent = createParents(names)
    val result = new File(parent, names.last)
		val successful = result.mkdir()
		if (!successful) throw new IOException("cannot create directory: " + result)
    result
  }

  private def createParents (names: Array[String]) = {
    var result = path
    for (name <- names.dropRight(1)) {
    	result = new File(result, name)
    	if (!result.exists()) {
    	  val successful = result.mkdir()
    	  if (!successful) throw new IOException("cannot create directory: " + result)
    	}
    }
    result
  }

  /**
   * Creates a new file within this temporary directory.
   *
   * If the `file` argument is empty, a unique file is created.
   *
   * If the `file` argument denotes a path with elements separated by any
   * characters defined by `Separators`, the whole path is created as necessary,
   * but the last element (the regular file) must not exist before.
   *
   * @param file
   * @return the target location
   * @throws IOException if the file cannot be created
   */
  @throws(classOf[IOException])
  def createFile (file: String): File = {
    if (!isInActiveState) throw new IllegalStateException("not in active state")
    if (file.isEmpty()) return File.createTempFile(SubPrefix, SubSuffix, path)
    val names = file.split(Separators)
    val parent = createParents(names)
    val result = new File(parent, names.last)
		val successful = result.createNewFile()
		if (!successful) throw new IOException("cannot create file: " + result)
    result
  }

  /**
   * Removes all files and directories added or created before.
   *
   * The root of this temporary directory itself is not deleted and it remains
   * in "active" state, which means that new files and directories still can be
   * added or created.
   *
   * @note Can only be successful if no files or directories have been added by
   * bypassing the methods of this object.
   *
   * @return `true` if successful, `false` otherwise
   * @throws IllegalStateException if this object is not in active state
   */
  def cleanUp (): Boolean = {
    if (!isInActiveState) throw new IllegalStateException("not in active state")
    var filesRemoved = Nil: List[File]
    for (f <- files) {
      val deleted = f.delete()
      if (deleted) filesRemoved ::= f
    }
    files = files diff filesRemoved
    files.isEmpty
  }

  /**
   * Removes all files and directories added before and the root of the
   * temporary directory itself.
   *
   * Sets this temporary directory to "inactive" state, which means that any
   * subsequent method calls other than `isInActiveState` will throw an
   * illegal-state exception.
   *
   * @note Can only be successful if no files or directories have been added by
   * bypassing the methods of this object.
   *
   * @return `true` if successful, `false` otherwise
   * @throws IllegalStateException if this object is not in active state
   */
  def destroy (): Boolean = {
    if (!isInActiveState) throw new IllegalStateException("not in active state")
    val result = cleanUp() && path.delete()
    _isInActiveState = false
    result
  }
}

object TemporaryDirectory {

  final val Separators = Array('/', '\\')
  private final val RootPrefix = "td_"
  private final val SubPrefix = "ts_"
  private final val SubSuffix = ".tmp"

  /**
   * Creates a new directory in the file system (either under the default
   * temporary-file directory or under the specified directory) and returns an
   * object to work with this directory.
   *
   * @param target optional target directory (default temporary-file directory
   * if `None`)
   * @return a new temporary directory instance
   * @throws java.io.IOException
   */
  @throws(classOf[IOException])
  def apply (target: Option[File] = None) = {
    target foreach { f: File => require(f.isDirectory, "target must be a directory") }
    new TemporaryDirectory(target.map(_.toPath()))
  }

}
