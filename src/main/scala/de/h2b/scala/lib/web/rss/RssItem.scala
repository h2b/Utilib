/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.web.rss

import java.net.URL
import java.text.DateFormat
import java.util.Date

import scala.util.Try
import scala.xml.{ Elem, Node }

/**
 * @see [[https://de.wikipedia.org/wiki/RSS_(Web-Feed)]]
 * @author h2b
 */
case class RssItem private (title: String, description: Option[String] = None,
    link: Option[URL] = None, author: Option[String] = None, guid: Option[String] = None,
    pubDate: Option[Date] = None) extends Item {

  def toElem (): Elem =
    <item>
      <title>{title}</title>
      { if (description.isDefined) <description>{description.get}</description> }
      { if (link.isDefined) <link>{link.get}</link> }
      { if (author.isDefined) <author>{author.get}</author> }
      { if (guid.isDefined) <guid>{guid.get}</guid> }
      { if (pubDate.isDefined) <pubDate>{pubDate.get}</pubDate> }
    </item>

}

object RssItem {

  val name: String = "item"

  /**
   * Converts an XML node to an RSS item.
   *
   * If a `link` child is present, but cannot be converted to an URL, the
   * corresponding RSS-item parameter becomes `None`.
   *
   * If a `pubDate` child is present, but cannot be converted to a date using
   * `dateFormat`, the corresponding RSS-item parameter becomes `None`.
   *
   * Apart from that, if a child corresponding to an RSS-item parameter is not
   * present or empty, the parameter becomes `None`. An exception is the
   * `title` parameter which becomes the empty string if not present or empty.
   *
   * @note No check is made if `node` is really an RSS item. Applying to an XML
   * node other than an RSS item yields a result like
   * `RssItem("", None, None, None, None, None)` or some other garbage but
   * throws no exception.
   *
   * @param node the XML node to be converted
   * @param dateFormat the format to be used on the `pubDate` child
   * @return the resulting RSS item
   */
  def fromNode (node: Node, dateFormat: DateFormat): RssItem = {
    val title = (node \ "title").text
    val description = Some((node \ "description").text) filter { _.length > 0 }
    val link = Try(new URL((node \ "link").text)).toOption
    val author = Some((node \ "author").text) filter { _.length > 0 }
    val guid = Some((node \ "guid").text) filter { _.length > 0 }
    val pubDate = Try(dateFormat.parse((node \ "pubDate").text)).toOption
    RssItem(title, description, link, author, guid, pubDate)
  }

}
