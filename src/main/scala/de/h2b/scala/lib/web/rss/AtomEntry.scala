/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.web.rss

import java.net.URL
import java.text.DateFormat
import java.util.Date

import scala.util.Try
import scala.xml.{ Elem, Node }

/**
 * @see [[https://de.wikipedia.org/wiki/Atom_(Format)]]
 *
 * @author h2b
 */
case class AtomEntry private (title: String, summary: Option[String] = None,
    content: Option[String] = None, link: Option[URL] = None, id: Option[String] = None,
    updated: Option[Date] = None) extends Item {

  def toElem (): Elem =
    <entry>
    	<title>{title}</title>
      { if (link.isDefined) <link>href="{link.get}"</link> }
    	{ if (id.isDefined) <id>{id.get}</id> }
    	{ if (updated.isDefined) <updated>{updated.get}</updated> }
    	{ if (summary.isDefined) <summary>{summary.get}</summary> }
    	{ if (content.isDefined) <content>{content.get}</content> }
  	</entry>

}

object AtomEntry {

  val name: String = "entry"

  /**
   * Converts an XML node to an Atom entry.
   *
   * The `link` child must be of the format `href="http://example.org"`.
   * If it is present, but cannot be converted to an URL, the
   * corresponding Atom-item parameter becomes `None`.
   *
   * If an `updated` child is present, but cannot be converted to a date using
   * `dateFormat`, the corresponding Atom-entry parameter becomes `None`.
   *
   * Apart from that, if a child corresponding to an Atom-entry parameter is not
   * present or empty, the parameter becomes `None`. An exception is the
   * `title` parameter which becomes the empty string if not present or empty.
   *
   * @note No check is made if `node` is really an Atom entry. Applying to an XML
   * node other than an Atom entry yields a result like
   * `AtomEntry("", None, None, None, None, None)` or some other garbage but
   * throws no exception.
   *
   * @param node the XML node to be converted
   * @param dateFormat the format to be used on the `updated` child
   * @return the resulting Atom entry
   */
  def fromNode (node: Node, dateFormat: DateFormat): AtomEntry = {
    val title = (node \ "title").text
    val summary = Some((node \ "summary").text) filter { _.length > 0 }
    val content = Some((node \ "content").text) filter { _.length > 0 }
    val link = Try(new URL(extractedLink(node))).toOption
    val id = Some((node \ "id").text) filter { _.length > 0 }
    val updated = Try(dateFormat.parse((node \ "updated").text)).toOption
    AtomEntry(title, summary, content, link, id, updated)
  }

  private def extractedLink (n: Node): String = {
    val regex = """href="(.*)"""".r //<link>href="http://example.org"</link>
    (n \ "link").text match {
      case regex(link) => link
      case _ => ""
    }
  }

}
