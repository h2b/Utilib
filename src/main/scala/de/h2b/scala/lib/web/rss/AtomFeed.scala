/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.web.rss

import java.io.{ File, IOException }
import java.text.DateFormat
import java.util.Date

import scala.util.Try
import scala.xml.{ Elem, Node, XML }

/**
 * @see [[https://de.wikipedia.org/wiki/Atom_(Format)]]
 * @author h2b
 */
case class AtomFeed private (title: String, authorName: Option[String] = None,
    id: Option[String] = None, updated: Option[Date] = None,
    items: Seq[AtomEntry] = Seq.empty) extends Feed[AtomFeed, AtomEntry] {

	def toElem (): Elem =
    <feed xmlns="http://www.w3.org/2005/Atom">
  		{
	      if (authorName.isDefined)
    		  <author>
    				<name>{authorName.get}</name>
  				</author>
  		}
  		<title>{title}</title>
  		{ if (id.isDefined) <id>{id.get}</id> }
  		{ if (updated.isDefined) <updated>{updated.get}</updated> }
      {
        for (i <- items) yield i.toElem()
      }
		</feed>

	def withStamp (date: Date): AtomFeed = copy(updated=Some(date))

	protected def copyWithItems (items: Seq[AtomEntry]): AtomFeed = copy(items=items)

	protected def lt (i: AtomEntry, j: AtomEntry): Boolean =
    if (i.updated.isDefined && j.updated.isDefined) i.updated.get.before(j.updated.get)
    else true

}

object AtomFeed {

  /**
   * Converts an XML node to an Atom feed.
   *
   * If an `updated` child is present, but cannot be converted to a date using
   * `dateFormat`, the corresponding Atom-feed parameter becomes `None`.
   *
   * Apart from that, if a child corresponding to an Atom-feed parameter is not
   * present or empty, the parameter becomes `None`. An exception is the
   * `title` parameter which becomes the empty string if not present or empty.
   *
   * @note No check is made if `node` is really an Atom feed. Applying to an XML
   * node other than an Atom feed yields a result like
   * `AtomFeed("", None, None, None, Seq.empty[AtomEntry])` or some other garbage but
   * throws no exception.
   *
   * @param node the XML node to be converted
   * @param dateFormat the format to be used on the `updated` child
   * @return the resulting Atom feed
   */
  def fromNode (node: Node, dateFormat: DateFormat): AtomFeed = {
    val title = (node \ "title").text
    val authorName = Some((node \ "author" \ "name").text) filter { _.length > 0 }
    val id = Some((node \ "id").text) filter { _.length > 0 }
    val updated = Try(dateFormat.parse((node \ "updated").text)).toOption
    val items = (node \ AtomEntry.name) map { (n: Node) => AtomEntry.fromNode(n, dateFormat) }
    AtomFeed(title, authorName, id, updated, items)
  }

  @throws(classOf[IOException])
  def load (file: File, dateFormat: DateFormat): AtomFeed = fromNode(XML.loadFile(file), dateFormat)

}
