/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.web.rss

import java.io.{ File, IOException }
import java.net.URL
import java.text.DateFormat
import java.util.Date

import scala.util.Try
import scala.xml.{ Elem, Node, XML }

/**
 * @see [[https://de.wikipedia.org/wiki/RSS_(Web-Feed)]]
 * @author h2b
 */
case class RssChannel private (title: String, description: Option[String] = None,
    link: Option[URL] = None, pubDate: Option[Date] = None,
    items: Seq[RssItem] = Seq.empty) extends Feed[RssChannel, RssItem] {

	def toElem (): Elem =
    <rss version="2.0">
		  <channel>
    		<title>{title}</title>
    		{ if (link.isDefined) <link>{link.get}</link> }
        { if (description.isDefined) <description>{description.get}</description> }
        { if (pubDate.isDefined) <pubDate>{pubDate.get}</pubDate> }
        {
          for (i <- items) yield i.toElem()
        }
      </channel>
    </rss>

	def withStamp (date: Date): RssChannel = copy(pubDate=Some(date))

  protected def copyWithItems (items: Seq[RssItem]): RssChannel = copy(items=items)

	protected def lt (i: RssItem, j: RssItem): Boolean =
    if (i.pubDate.isDefined && j.pubDate.isDefined) i.pubDate.get.before(j.pubDate.get)
    else true

}

object RssChannel {

  val nestedName = "channel"

  /**
   * Converts an XML node to an RSS channel.
   *
   * If a `link` child is present, but cannot be converted to an URL, the
   * corresponding RSS-channel parameter becomes `None`.
   *
   * If a `pubDate` child is present, but cannot be converted to a date using
   * `dateFormat`, the corresponding RSS-channel parameter becomes `None`.
   *
   * Apart from that, if a child corresponding to an RSS-channel parameter is not
   * present or empty, the parameter becomes `None`. An exception is the
   * `title` parameter which becomes the empty string if not present or empty.
   *
   * @note No check is made if `node` is really an RSS channel. Applying to an XML
   * node other than an RSS channel yields a result like
   * `RssChannel("", None, None, None, Seq.empty[RssItem])` or some other garbage but
   * throws no exception.
   *
   * @param node the XML node to be converted
   * @param dateFormat the format to be used on the `pubDate` child
   * @return the resulting RSS channel
   */
  def fromNode (node: Node, dateFormat: DateFormat): RssChannel = {
    val title = (node \ nestedName \ "title").text
    val link = Try(new URL((node \ nestedName \ "link").text)).toOption
    val description = Some((node \ nestedName \ "description").text) filter { _.length > 0 }
    val pubDate = Try(dateFormat.parse((node \ nestedName \ "pubDate").text)).toOption
    val items = (node \ nestedName \ RssItem.name) map { (n: Node) => RssItem.fromNode(n, dateFormat) }
    RssChannel(title, description, link, pubDate, items)
  }

  @throws(classOf[IOException])
  def load (file: File, dateFormat: DateFormat): RssChannel = fromNode(XML.loadFile(file), dateFormat)

}
