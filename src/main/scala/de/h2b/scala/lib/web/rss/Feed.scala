/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.web.rss

import java.io.{ File, IOException }
import java.util.Date

import scala.io.Codec
import scala.xml.{ Elem, XML }

trait Feed [F <: Feed[F, I], I <: Item] {

	val items: Seq[I]

  /**
	 * Converts this feed to an XML element.
	 *
	 * Feed parameters equal to `None` will not be included in the XML element.
	 *
	 * @return the resulting XML element
	 */
	def toElem (): Elem

	/**
	 * A copy of this feed with a new time stamp, if available. It is
	 * implementation dependent, what happens if this feed has no time-stamp
	 * parameter or multiple ones, but in any case you get some -- maybe
	 * identical -- copy of this feed.
	 *
	 * @param date the new time stamp
	 * @return a copy of this feed with a new time stamp
	 */
	def withStamp (date: Date): F

	object withItems {

		/**
		 * @return a copy of this feed with its items replaced by the specified ones
		 */
		def replaced (items: Seq[I]): F = copyWithItems(items)

		/**
		 * @return a copy of this feed with the specified items appended to its items
		 */
  	def appended (items: Seq[I]): F = copyWithItems(Feed.this.items ++ items)

		/**
		 * @return a copy of this feed with the specified items prepended to its items
		 */
  	def prepended (items: Seq[I]): F = copyWithItems(items ++ Feed.this.items)

		/**
		 * @return a copy of this feed with the specified items removed from its items
		 */
  	def removed (items: Seq[I]): F = copyWithItems(Feed.this.items.filterNot(items.contains(_)))

		/**
		 * @return a copy of this feed with all items removed
		 */
  	def removedAll (): F = copyWithItems(Seq.empty)

		/**
		 * @return a copy of this feed with its items in reversed order
		 */
  	def reversed (): F = copyWithItems(Feed.this.items.reverse)

		/**
  	 * @param lt a function that defines the order of two items (is the first
  	 * one before the other?)
  	 * @return a copy of this feed with its items in sorted order
  	 */
  	def sorted (lt: (I, I) => Boolean): F = copyWithItems(Feed.this.items.sortWith(lt))

  	/**
  	 * @note It is implementation dependent, how items without date or items with
  	 * multiple dates are sorted in.
  	 */
  	def sortedByStampAscending () = sorted(lt)

  	/**
  	 * @note It is implementation dependent, how items without date or items with
  	 * multiple dates are sorted in.
  	 */
  	def sortedByStampDescending () = sorted(!lt(_, _))

	}

	protected def copyWithItems (items: Seq[I]): F

	protected def lt (i: I, j: I): Boolean

  @throws(classOf[IOException])
  def save (file: File, encoding: Codec = Codec.UTF8): Unit = {
    if (file.exists()) Feed.delete(file)
    XML.save(file.getPath, toElem(), encoding.name, xmlDecl=true)
  }

}

object Feed {

  @throws(classOf[IOException])
  def delete (file: File): Unit = {
    val done = file.delete()
    if (!done) throw new IOException(s"cannot delete file: $file")
  }

}
