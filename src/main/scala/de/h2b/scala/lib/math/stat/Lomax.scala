/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.stat

import scala.math._

object Lomax {

  //Note: This is what Sedgewick's StdRandom implements as Pareto(alpha).
  def apply (alpha: Double) = new Lomax(alpha, 1.0)

  def apply (alpha: Double, lambda: Double) = new Lomax(alpha, lambda)

}

/**
 * Generates values conforming to a Lomax (shifted Pareto) distribution with
 * shape parameter α and scala parameter λ.
 *
 * @see [[https://en.wikipedia.org/wiki/Lomax_distribution]]
 *
 * @constructor
 *
 * @param alpha shape parameter α (must be positive)
 * @param scale parameter λ (must be positive)
 * @throws IllegalArgumentException if alpha or xmin not positive
 *
 * @author h2b
 *
 */
class Lomax private (alpha: Double, lambda: Double) extends Rng[Double] {

  require(alpha>0, s"shape parameter not positive: alpha=$alpha")
  require(lambda>0, s"scale parameter not positive: lambda=$lambda")

  val pareto = Pareto(alpha, lambda)

  def next () = pareto.next()-lambda

}