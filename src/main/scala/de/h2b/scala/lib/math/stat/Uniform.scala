/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.stat

/**
 * Generates uniformly distributed values.
 *
 * @author h2b
 *
 */
object Uniform {

  def apply () = new DoubleUniform(0.0, 1.0)

  def apply (a: Double, b: Double) = new DoubleUniform(a, b)

  def apply (n: Long) = new LongUniform(0, n)

  def apply (a: Long, b: Long) = new LongUniform(a, b)

  def apply (n: Int) = new IntUniform(0, n)

  def apply (a: Int, b: Int) = new IntUniform(a, b)

}

/**
 * Generates uniformly distributed {@code Double} values in `[a, b)`.
 *
 * @constructor
 *
 * @param a
 * @param b
 * @throws IllegalArgumentException if not a less than b
 *
 * @author h2b
 */
class DoubleUniform private[stat] (a: Double, b: Double) extends Rng[Double] {

  require(a<b, s"invalid range: [$a, $b)")

  def next () = a + random.nextDouble() * (b-a)

}

/**
 * Generates uniformly distributed {@code Long} values in `{a, b-1}`.
 *
 * @constructor
 *
 * @param a
 * @param b
 * @throws IllegalArgumentException if not a less than b
 *
 * @author h2b
 */
class LongUniform private[stat] (a: Long, b: Long) extends Rng[Long] {

  require(a<b, s"invalid range: [$a, $b)")

	def next () = a + (random.nextDouble() * (b-a)).floor.toLong

}

/**
 * Generates uniformly distributed {@code Int} values in `{a, b-1}`.
 *
 * @constructor
 *
 * @param a
 * @param b
 * @throws IllegalArgumentException if not a less than b
 *
 * @author h2b
 */
class IntUniform private[stat] (a: Int, b: Int) extends Rng[Int] {

  require(a<b, s"invalid range: [$a, $b)")

  def next () = a + (random.nextDouble() * (b-a)).floor.toInt

}
