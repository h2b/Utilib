/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math

/**
 * An object that defines fundamental mathematical constants.
 *
 * The values of the constants are taken from Abramowitz/Stegun, pp. 2-3. They
 * have as much digits as listed there, respectively.
 *
 * Beyond, there are two useful constants for conversion from radians to
 * degrees and vice versa.
 *
 * Copyright (C) 1991, 2006, 2008, 2015 Hans-Hermann Bode
 *
 * @note We use lowercase identifiers deliberately to reflect the usual
 *       mathematical notation and provide addtional Unicode greek-letter
 *       versions where appropriate.
 *
 * @see M. Abramowitz and I. A. Stegun (Ed.). Handbook of Mathematical Functions.
 *      Dover Publications, New York (1964). 9th printing, 1970.
 *
 * @author h2b
 * @version 3.0
 */
object Constants {

    /**
     * e.
     */
    final val e = 2.718281828459045235360287

    /**
     * π.
     */
    final val pi = 3.141592653589793238462643
    final val π = pi

    /**
     * Euler constant γ.
     */
    final val gamma = 0.577215664901532860606512
    final val γ = gamma

    /**
     * √2.
     */
    final val sqrt2 = 1.4142135623730950488

    /**
     * √10.
     */
    final val sqrt10 = 3.1622776601683793320

    /**
     * √e.
     */
    final val sqrtE = 1.6487212707001281468

    /**
     * √π.
     */
    final val sqrtPi = 1.772453850905516027298167
    final val sqrtπ = sqrtPi

    /**
     * e^π^.
     */
    final val expPi = 23.140692632779269006
    final val expπ = expPi

    /**
     * e^γ^.
     */
    final val expGamma = 1.7810724179901979852
    final val expγ = expGamma

    /**
     * ln 2.
     */
    final val ln2 = 0.6931471805599453094172321

    /**
     * ln 10.
     */
    final val ln10 = 2.3025850929940456840179915

    /**
     * ln π.
     */
    final val lnPi = 1.144729885849400174143427
    final val lnπ = lnPi

    /**
     * ln γ.
     */
    final val lnGamma = -0.549539312981644822337662
    final val lnγ = lnGamma

    /**
     * 1 rad in deg.
     */
    final val rad = (180.0 / pi)

    /**
     * 1 deg in rad.
     */
    final val deg = (1.0 / rad)

}