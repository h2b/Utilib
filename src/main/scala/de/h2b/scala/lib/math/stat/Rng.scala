/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.stat

import scala.util.Random

/**
 * Defines a basic random-number-generator trait to be used by various concrete
 * generators implementing specific probability distributions.
 *
 * @author h2b
 *
 * @param <A> the codomain of the generator function
 */
trait Rng [A] {

  protected val random = new Random()

  def setSeed (seed: Long) = random.setSeed(seed)

  /**
   * @return the next value drawn from this distribution
   */
  def next (): A

}