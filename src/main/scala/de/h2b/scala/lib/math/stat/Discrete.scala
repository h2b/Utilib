/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.stat

object Discrete {

  def apply (a: Array[Double]) = new Discrete(a)

}

/**
 * Draws values from a discrete distribution given by an array a of
 * probabilities.
 *
 * More precise, if n=a.length, a value i with i in {0, ..., n-1} is drawn
 * with probability a,,i,,.
 *
 * @constructor
 *
 * @param a array of probabilities: each value must be nonnegative and all
 * values must sum up to 1
 * @throws IllegalArgumentException if an element of a is negative or the sum
 * over all elements of a is not 1 (within machine accuracy)
 *
 * @author h2b
 *
 */
class Discrete private (a: Array[Double]) extends Rng[Int] {

  //implementation taken from the discrete method in Sedgewick's StdRandom.java

  final val epsilon = 5e-16 //actual machine epsilon is 2.220446049250313E-16

  checkRequirements()

  private def checkRequirements () = {
    var sum = 0.0
    for (i <- 0 until a.length) {
      val ai = a(i)
      require(ai>=0, s"negative probability for index $i: $ai")
      sum += ai
    }
    require(1-epsilon<sum && sum<1+epsilon, s"sum of probabilities is $sum (must be 1)")
  }

  val uniform = Uniform()

  def next (): Int = {
    val u = uniform.next()
    var sum = 0.0
    for (i <- 0 until a.length) {
      sum += a(i)
      if (sum>u) return i
    }
    //If we get here, u is closer to 1 than sum (due to roundoff errors).
    //Differently from Sedgwick's implementation we simply return the highest
    //index in this case.
    a.length-1
  }

}