/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.stat

import scala.math._

object Lorentz {

  def apply () = Cauchy()

}

object Cauchy {

  def apply () = new Cauchy()

}

/**
 * Generates values conforming to a standard Cauchy (or Lorentz) distribution.
 *
 * @see [[https://de.wikipedia.org/wiki/Cauchy-Verteilung]]
 *
 * @author h2b
 *
 */
class Cauchy private extends Rng[Double] {

  val uniform = Uniform()

  def next () = {
    var u = 0.0
    do {
      u = uniform.next()
    } while (u==0.0)
    tan(u-Pi/2)
  }

}