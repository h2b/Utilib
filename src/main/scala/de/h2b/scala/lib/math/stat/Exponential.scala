/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.stat

import scala.math._

object Exponential {

  def apply (lambda: Double) = new Exponential(lambda)

}

/**
 * Generates values conforming to an exponential distribution with rate
 * parameter λ.
 *
 * @see [[https://de.wikipedia.org/wiki/Exponentialverteilung]]
 *
 * @constructor
 *
 * @param lambda rate parameter λ (must be positive)
 * @throws IllegalArgumentException if lambda less than or equal to 0
 *
 * @author h2b
 */
class Exponential private (lambda: Double) extends Rng[Double] {

  require(lambda>0, s"rate parameter not positive: lambda=$lambda")

	val uniform = Uniform()

	def next () = -log(1-uniform.next())/lambda

}