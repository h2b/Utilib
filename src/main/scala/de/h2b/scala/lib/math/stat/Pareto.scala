/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.stat

import scala.math._

object Pareto {

  def apply (alpha: Double, xmin: Double) = new Pareto(alpha, xmin)

}

/**
 * Generates values conforming to a Pareto distribution with shape parameter α
 * and minimum value x,,min,,.
 *
 * @see [[https://en.wikipedia.org/wiki/Pareto_distribution]]
 *
 * @constructor
 *
 * @param alpha shape parameter α (must be positive)
 * @param xmin minimum value x,,min, (must be positive)
 * @throws IllegalArgumentException if alpha or xmin not positive
 *
 * @author h2b
 *
 */
class Pareto private (alpha: Double, xmin: Double) extends Rng[Double] {

  require(alpha>0, s"shape parameter not positive: alpha=$alpha")
  require(xmin>0, s"minimum value not positive: xmin=$xmin")

  val uniform = Uniform()

  def next () = xmin * pow(1.0-uniform.next(), -1.0/alpha)

}