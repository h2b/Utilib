/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.stat

import scala.math._

object Poisson {

  def apply (lambda: Double) = new Poisson(lambda)

}

/**
 * Generates values conforming to a Poisson distribution P,,λ,,(k) =
 * (λ^k^/k!)e^-λ^.
 *
 * The Poisson distribution yields the probability of the occurence of k events
 * within a certain period of time, assuming that λ denotes the average
 * occurence of events during that period and privided that events are
 * independent of the time since the last event.
 *
 * It turns out that λ is both the expected value and the variance of the
 * distribution.
 *
 * @see [[https://de.wikipedia.org/wiki/Poisson-Verteilung]]
 *
 * @constructor
 *
 * @param lambda λ, must be positive and finite
 * @throws IllegalArgumentException if lambda not positive or not finite
 *
 * @author h2b
 */
class Poisson private (lambda: Double) extends Rng[Int] {

  require(lambda>0, s"lambda not positive: $lambda")
  require(!lambda.isPosInfinity, s"lambda not finite: $lambda")

  val uniform = Uniform()

  def next () = {
    //simple algorithm given by Knuth
    //see https://en.wikipedia.org/wiki/Poisson_distribution#Generating_Poisson-distributed_random_variables
	  val L = exp(-lambda)
    var k = 0
    var p = 1.0
    do {
      k += 1
      val u = uniform.next()
      p *= u
    } while (p>L)
    k-1
  }

}