/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib

package object math {

	/**
	 * Smallest positive {@code Double} value that fulfills
	 * {@code 1.0 + smallDouble > 1.0} and {@code 1.0 - smallDouble < 1.0}.
	 * (Approx.)
	 */
	final val smallDouble = 2.22044604925031308e-16

	/**
	 * Smallest positive {@code Float} value that fulfills
	 * {@code 1.0 + smallFloat > 1.0} and {@code 1.0 - smallFloat < 1.0}.
	 * (Approx.)
	 */
	final val smallFloat = 1.19209290e-7F

	trait Tolerance [A] { val epsilon: A }

  class DoubleTolerance (val epsilon: Double) extends Tolerance[Double]

  class FloatTolerance (val epsilon: Float) extends Tolerance[Float]

  implicit val doubleTolerance = new DoubleTolerance(smallDouble)

	implicit val floatTolerance = new FloatTolerance(smallFloat)

  implicit class DoubleApproxEqual (x: Double) (implicit val tolerance: Tolerance[Double]) {
		/**
		 * Compares values within tolerance.
		 *
		 * @see [[http://alvinalexander.com/scala/how-to-compare-floating-point-numbers-in-scala-float-double]]
		 */
		def ~= (y: Double): Boolean = (x-y).abs<tolerance.epsilon
  }

  implicit class FloatApproxEqual (x: Float) (implicit val tolerance: Tolerance[Float]) {
		/**
		 * Compares values within tolerance.
		 *
		 * @see [[http://alvinalexander.com/scala/how-to-compare-floating-point-numbers-in-scala-float-double]]
		 */
		def ~= (y: Float): Boolean = (x-y).abs<tolerance.epsilon
  }

}
