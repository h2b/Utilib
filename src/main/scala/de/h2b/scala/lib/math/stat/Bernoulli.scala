/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.stat

/**
 * Generates {@code p}-distributed Boolean values.
 *
 * @author h2b
 *
 */
object Bernoulli {

  def apply () = new Bernoulli(0.5)

  def apply (p: Double) = new Bernoulli(p)

}

/**
 * Boolean distribution: generates {@code true} with probability {@code p} and
 * {@code false} with probabilty {@code 1-p}.
 *
 * @constructor
 *
 * @param p
 * @throws IllegalArgumentException if p not in `[0, 1]`
 *
 * @author h2b
 */
class Bernoulli private (p: Double) extends Rng[Boolean] {

  require(0.0<=p && p<=1.0, s"invalid probability: $p not in [0, 1]")

  def next () = random.nextDouble() < p

}