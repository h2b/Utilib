/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.stat

import scala.math._

object Gaussian {

  def apply () = new Gaussian(0.0, 1.0)

  def apply (mean: Double, stddev: Double) = new Gaussian(mean, stddev)

}

/**
 * Generates values conforming to a normal (or Gaussian) distribution with
 * given mean and standard deviation
 *
 * @constructor
 *
 * @param mean
 * @param stddev
 *
 * @author h2b
 */
class Gaussian private (mean: Double, stddev: Double) extends Rng[Double] {

  val uniform = Uniform(-1.0, 1.0)

  def next () = {
    //implementing the Marsaglia polar method (modified Box-Muller algorithm)
    //see https://de.wikipedia.org/wiki/Gaussverteilung
    var u1, u2, q = 0.0;
    do {
      u1 = uniform.next()
      u2 = uniform.next()
      q = u1*u1+u2*u2
    } while (q>1 || q==0)
    val p = sqrt(-2 * log(q) / q)
    val x1 = u1*p; //discarding second independent value u2*p
    mean+stddev*x1
  }

}