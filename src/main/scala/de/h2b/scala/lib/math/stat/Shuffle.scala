/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.stat

/**
 * Randomly shuffles the elements of an array in place.
 *
 * @see [[https://en.wikipedia.org/wiki/Fisher%E2%80%93Yates_shuffle]]
 *
 * @author h2b
 *
 */
object Shuffle {

  val uniform = Uniform()

  def apply [E] (a: Array[E]): Unit = {
	  val n = a.length
	  for (i <- n-1 to 1 by -1) {
	    val j = (uniform.next()*(i+1)).floor.toInt
	    swap(a, i, j)
	  }
  }

  private def swap [E] (a: Array[E], i: Int, j: Int): Unit = {
    val ai = a(i)
    a(i) = a(j)
    a(j) = ai
  }

}