/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.phys

import scala.util.Try

/**
 * Provides normalized time units based on milliseconds.
 *
 * @author h2b
 *
 * @param ms millseconds (non-negative value)
 * @throws IllegalArgumentException if ms is negative
 */
class Time private (val ms: Long) extends Equals {

	require(ms>=0, "negative value for milliseconds: " + ms)

	/** Total seconds. */
	val s = ms.toDouble / Time.s

	private var value = ms
	/** Fraction of days. */
	val days = value / Time.d; value %= Time.d
	/** Fraction of hours. */
	val hours = value / Time.h; value %= Time.h
	/** Fraction of minutes. */
	val minutes = value / Time.m; value %= Time.m
	/** Fraction of seconds. */
	val seconds = value / Time.s; value %= Time.s
	/** Fraction of milliseconds. */
  val milliseconds = value

  /**
   * Converts this time to a convenient string representation.
   *
   * @return the string
   */
  override def toString () = {
    val builder = new StringBuilder
    if (days>0) builder ++= days.toString() + "d"
    if (hours>0) builder ++= hours.toString() + "h"
    if (minutes>0) builder ++= minutes.toString() + "m"
    if (seconds>0) builder ++= seconds.toString() + "s"
    if (milliseconds>0 || builder.length()==0) builder ++= milliseconds.toString() + "ms"
    builder.result
  }

  def canEqual(other: Any) = other.isInstanceOf[Time]

  override def equals(other: Any) =
	  other match {
	    case that: Time => that.canEqual(this) && ms == that.ms
	    case _ => false
	  }

  override def hashCode() = {
	  val prime = 41
	  prime + ms.hashCode
	}

}

/**
 * @author h2b
 */
object Time {

  final val ms = 1L;
  final val s = 1000*ms;
  final val m = 60*s;
  final val h = 60*m;
  final val d = 24*h;

  final val μs = ms/1000.0;
  final val ns = μs/1000.0;

  private val timeRegex = """(?:(\d+)d)?(?:(\d+)h)?(?:(\d+)m)?(?:(\d+)s)?(?:(\d+)ms)?""".r

  def apply (ms: Long): Time = new Time(ms)

  /**
   * The argument is composed of a sequence of values each followed by a time unit:
   * "d" for days, "h" for hours, "m" for minutes, "s" for seconds and "ms" for
   * milliseconds. The general format is: "xdxhxmxsxms", where "x" denotes a
   * number (a sequence of digits). Each number/unit part is optional. The
   * unit identifiers are case sensitive (lower case is mandatory). No
   * whitespace is allowed.
   *
   * @example Time("3d25m") //3 days and 25 minutes
   * @example Time("5m12s125ms") //5 minutes, 12 seconds and 125 milliseconds
   *
   * @param str the string to be converted
   * @return a new time converted from the string
   * @throws IllegalArgumentException if the string cannot be converted to `Time`
   */
  @throws(classOf[IllegalArgumentException])
  def apply (str: String): Time =
    str match {
      case timeRegex(dStr,hStr,mStr,sStr,msStr) =>
        val dVal= Try(dStr.toLong).getOrElse(0L)
        val hVal = Try(hStr.toLong).getOrElse(0L)
        val mVal = Try(mStr.toLong).getOrElse(0L)
        val sVal = Try(sStr.toLong).getOrElse(0L)
        val msVal = Try(msStr.toLong).getOrElse(0L)
        new Time(dVal*d+hVal*h+mVal*m+sVal*s+msVal*ms)
      case _ =>
        throw new IllegalArgumentException(s"Argument cannot be converted to Time: $str")
    }

  def unapply (t: Time): Option[Long] = Some(t.ms)

}
