/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util.config

import java.io.{ File, FileInputStream, IOException }
import java.util.Properties

import scala.collection.immutable.Map
import scala.util.{ Failure, Success, Try }

import de.h2b.scala.lib.io.getResource
import de.h2b.scala.lib.util.Logger
import de.h2b.scala.lib.util.JavaCollectionConverters._

/**
 * A `Config` that loads key/value pairs from a single properties file.
 *
 * @constructor
 *
 * @param propFile the file from which the key/value pairs are loaded from
 *
 * @note  The content of the file must conform to the
 * `java.util.Properties#load(InputStream)` method.
 *
 * @note If some I/O or other exception occurs when loading the file, the
 * resulting `Config` is empty.
 *
 * @author h2b
 */
class FileConfig private (private val propFile: File) extends Config[String, String] {

  private val log = Logger(classOf[FileConfig])

  protected val props: Map[String, String] = javaProperties().asScala.toMap

  private def javaProperties(): Properties = {
    val p = new Properties()
    Try(new FileInputStream(propFile)) match {
      case Success(in) =>
        try {
          p.load(in)
        } catch {
          case e: Throwable => log.error(s"error loading properties file: ${e}")
        } finally {
          Try(in.close())
        }
      case Failure(e) =>
        log.error(s"missing properties file: ${e}")
    }
    p
  }

}

object FileConfig {

  private val log = Logger(classOf[FileConfig])

  /**
   * @param propFile the file from which the key/value pairs are loaded from
   * @return a `Config` with key/value pairs loaded from a single properties file
   *
   * @note  The content of the file must conform to the
   * `java.util.Properties#load(InputStream)` method.
   *
   * @note If some I/O or other exception occurs when loading the file, the
   * resulting `Config` is empty.
   */
  def apply (propFile: File): Config[String, String] = new FileConfig(propFile)

  /**
   * Loads key/value pairs from a resource file of specified name associated
   * with a class.
   *
   * The resource is looked up under the modified package name of the given class
   * (i.e.,  with '/' substituted for '.'), unless the resource name begins with
   * a '/', in which case it is looked up under the resource name itself
   * (stripping the leading '/').
   *
   * @param c the class for which the resource shall be got
   * @param name the name of the desired resource
   * @return a `Config` with key/value pairs loaded from the given resource file
   *
   * @note  The content of the file must conform to the
   * `java.util.Properties#load(InputStream)` method.
   *
   * @note If some I/O or other exception occurs when loading the file, the
   * resulting `Config` is empty.
   */
  def apply (c: Class[_], name: String): Config[String, String] =
    Try(getResource(c, name)) match {
      case Success(file) => apply(file)
      case Failure(e) => log.error(s"missing resource file: ${e}"); Config.empty
    }

}
