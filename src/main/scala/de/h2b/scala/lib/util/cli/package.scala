/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util

/**
 * Define the parameters of your command line as instances of
 *
 * - `FlagParameter`: no specific value, just signals that it is there,
 *
 * - `HelpParameter`: signals that the user needs some help (if given as a
 * command-line argument, other parameters are not computed and no parse
 * exceptions are thrown),
 *
 * - `ValueParameter[V]`: has exactly one value of type `V`,
 *
 * - `ListParameter[V]`: has a number of values of type `V` and
 *
 * - `MainParameter`: has a number of `String` values.
 *
 * `ValueParameter[V]` and `ListParameter[V]` need a converter to convert a
 * string to the value type `V`. You might want to import the object
 * `Converter` that provides converters for standard data types implicitly by:
 * {{{
 *  import de.h2b.scala.lib.util.cli.Converter._
 * }}}
 *
 * Then create a new instance of `CommandLine` with a set of parameters. The
 * resulting object then can parse a string sequence or an array of strings
 * for parameters and their values. If something goes wrong, either a
 * `ParameterException` (some argument does not obey to the format specified
 * by its parameter) or a `CommandLineException` (something is wrong with the
 * arguments as a whole) is thrown.
 *
 * Finally query the original parameter instances for its `value` or `values`
 * field.
 *
 * The `usage` method of `CommandLine` constructs a string suitable for a
 * usage message.
 *
 * @example {{{
 *   import de.h2b.scala.lib.util.cli._
 *   import de.h2b.scala.lib.util.cli.Converter._
 *   val overwrite = FlagParameter(Set("-o", "--overwrite"), "overwrite target")
 *   val mode = ValueParameter(Set("-m"), "mode", default=Some(0))
 *   val main = MainParameter("source target", arity=2)
 *   CommandLine(Set(overwrite, mode, main)).parse("-o -m 1 from to".split(' '))
 *   println(overwrite.value) //> Some(true)
 *   println(mode.value) //> Some(1)
 *   println(main.values) //> Some(WrappedArray(from, to))
 *}}}
 */
package object cli {

}
