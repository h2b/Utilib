/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util.config

import java.util.{ Locale, ResourceBundle }

import scala.collection.immutable.Map
import scala.collection.mutable.{ Map => MutableMap }
import scala.util.{ Failure, Success, Try }

import de.h2b.scala.lib.util.Logger
import de.h2b.scala.lib.util.JavaCollectionConverters._

/**
 * A `Config` that loads key/value pairs from a resource bundle
 *
 * @constructor
 *
 * @param baseName base name of the resource bundle, a fully qualified class name
 * @param localeOption optional locale for which a resource bundle is desired;
 * for `None`, the default locale is used (as returned by `Locale.getDefault`)
 *
 * @note The base name must identify a resource bundle according to the
 * `java.util.ResourceBundle` class.
 *
 * @note All values must be strings.
 *
 * @note If some I/O or other exception occurs when loading the bundle, the
 * resulting `Config` is empty.
 *
 * @author h2b
 */
class BundleConfig private (private val baseName: String,
    private val localeOption: Option[Locale]) extends Config[String, String] {

  private val log = Logger(classOf[BundleConfig])

  private val locale = localeOption match {
    case Some(l) => l
    case None => Locale.getDefault
  }

  protected val props: Map[String, String] = bundleMap()

  private def bundleMap (): Map[String, String] = {
	  val builder = MutableMap.empty[String, String]
	  Try(ResourceBundle.getBundle(baseName, locale)) match {
	    case Success(bundle) =>
	      for (key <- bundle.keySet().asScala)
	        try {
	        	builder += key->bundle.getString(key)
	        } catch {
	          case e: Throwable => log.error(s"error getting value for key $key: ${e}")
	        }
	    case Failure(e) =>
	      log.error(s"missing resource bundle: ${e}")
	  }
	  builder.toMap
  }

}

object BundleConfig {

	/**
	 * @param baseName base name of the resource bundle, a fully qualified class name
	 * @param localeOption optional locale for which a resource bundle is desired;
	 * for `None`, the default locale is used (as returned by `Locale.getDefault`);
	 * defaults to `None`
	 * @return a `Config` with key/value pairs loaded from a resource bundle
	 *
	 * @note The base name must identify a resource bundle according to the
	 * `java.util.ResourceBundle` class.
	 *
	 * @note All values must be strings.
	 *
	 * @note If some I/O or other exception occurs when loading the bundle, the
	 * resulting `Config` is empty.
	 */
	def apply (baseName: String, localeOption: Option[java.util.Locale] = None): Config[String, String] =
    new BundleConfig(baseName, localeOption)

	/**
   * Loads key/value pairs from a resource bundle associated with a class.
   *
   * The bundle is looked up under the modified package name of the given class
   * (i.e.,  with '/' substituted for '.'). The base name of the bundle is equal
   * to the the name of the given class. If the bundle consists of files, each
   * must have an extension `.properties`.
   *
	 * @param c the class for which the resource bundle shall be got
	 * @param localeOption optional locale for which a resource bundle is desired;
	 * for `None`, the default locale is used (as returned by `Locale.getDefault`);
	 * defaults to `None`
	 * @return a `Config` with key/value pairs loaded from a resource bundle
	 *
	 * @note The base name must identify a resource bundle according to the
	 * `java.util.ResourceBundle` class.
	 *
	 * @note All values must be strings.
	 *
	 * @note If some I/O or other exception occurs when loading the bundle, the
	 * resulting `Config` is empty.
	 */
	def forClass (c: Class[_], localeOption: Option[java.util.Locale] = None): Config[String, String] =
    new BundleConfig(c.getName, localeOption)

}
