/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util.cli

import scala.util.{ Success, Try }
import de.h2b.scala.lib.util.StringOps

sealed trait Parameter [V] {

  def names: Set[String]
  def description: String

  /**
   * Specification of values following this parameter name:
   *
   * - positive number: exactly this number of values must follow this parameter name
   *
   * - negative number: at least abs(this number) of values must follow this
   * parameter name (variable arity)
   *
   * - zero: no values must follow this parameter name
   *
   * Values following this parameter name will be consumed up to the next
   * parameter.
   */
  def arity: Int

  def required: Boolean

  val hidden: Boolean = false

  /**
   * A usage message of this parameter in a two-column layout.
   *
   * The first column shows the names in one line, the second column shows
   * one ore more lines of description. The lines of the description are taken
   * as given, no automatic word wrap is done.
   *
   * @param namesColWidth minimum width of the names column (defaults to 0)
   * @return the usage message
   */
  def usage (namesColWidth: Int = 0): String = {
    if (hidden) return ""
    val paddingChar = ' '
    val nameSeparator = ", "
    val columnSeparator = if (required) " * " else "   "
    val builder = new StringBuilder()
    builder ++= names.mkString(nameSeparator).padTo(namesColWidth, paddingChar)
    builder ++= columnSeparator
    builder ++= lines(paddingChar.toString * builder.length)
    builder.result()
  }

  private def lines (leftMargin: String): String = {
    val linesSeq = description.linesWithoutSeparators.toSeq
    if (linesSeq.isEmpty) return ""
    val eol = '\n'
    val builder = new StringBuilder(linesSeq.head + eol)
    for (line <- linesSeq.tail) builder ++= leftMargin + line + eol
    builder.result()
  }

}

trait SingleValue [V] { self: Parameter[V] =>

  def default: Option[V]

  private[cli] var _value: Option[V] = None

  def value: Option[V] = _value orElse default

}

trait MultipleValues [V] { self: Parameter[V] =>

  def defaults: Option[Seq[V]]

  private[cli] var _values: Option[Seq[V]] = None

  def values: Option[Seq[V]] = _values orElse defaults

}

trait Converter [V] {

  def convert: (String) => Try[V]

}

object Converter {

  def apply [V] (f: (String) => Try[V]): Converter[V] = new Converter[V] {
		val convert: (String) => Try[V] = f
  }

  type ConverterFunction [V] = (String) => Try[V]

  implicit val convertToBoolean: ConverterFunction[Boolean] = (str: String) => Try(str.toBoolean)
  implicit val convertToByte: ConverterFunction[Byte] = (str: String) => Try(str.toByte)
  implicit val convertToShort: ConverterFunction[Short] = (str: String) => Try(str.toShort)
  implicit val convertToInt: ConverterFunction[Int] = (str: String) => Try(str.toInt)
  implicit val convertToLong: ConverterFunction[Long] = (str: String) => Try(str.toLong)
  implicit val convertToFloat: ConverterFunction[Float] = (str: String) => Try(str.toFloat)
  implicit val convertToDouble: ConverterFunction[Double] = (str: String) => Try(str.toDouble)
  implicit val convertToString: ConverterFunction[String] = (str: String) => Success(str)

}

/**
 * A flag parameter has no specific value, it just signals that it is there.
 *
 * If its `value` is defined, it is present in the command line, else it is not.
 *
 * @author h2b
 */
case class FlagParameter (names: Set[String], description: String) extends
    Parameter[Boolean] with SingleValue[Boolean] {

  val arity: Int = 0
  val required: Boolean = false
  val default: Option[Boolean] = None

}

/**
 * Signals that the user needs some help.
 *
 * If a help-parameter name is given as a command-line argument, other
 * parameters are not computed and no parse exceptions are thrown.
 *
 * If its `value` is defined, it is present in the command line, else it is not.
 *
 * @author h2b
 */
case class HelpParameter (names: Set[String]) extends Parameter[Boolean] with SingleValue[Boolean] {

  val description: String = "show this help"
  val arity: Int = 0
  val required: Boolean = false
  val default: Option[Boolean] = None

}

/**
 * A value parameter has exactly one value.
 *
 * The value can be retrieved by the `value` option.
 *
 * @author h2b
 */
case class ValueParameter [V] (names: Set[String], description: String, required: Boolean = false,
    default: Option[V] = None) (implicit val convert: (String) => Try[V]) extends
    Parameter[V] with SingleValue[V] with Converter[V] {

  val arity: Int = 1

}

/**
 * A list parameter has a number of values specified by its `arity`.
 *
 * The values can be retrieved as a sequence by the `values` option.
 *
 * @author h2b
 */
case class ListParameter [V] (names: Set[String], description: String, arity: Int,
    required: Boolean = false,
    defaults: Option[Seq[V]] = None) (implicit val convert: (String) => Try[V]) extends
    Parameter[V] with MultipleValues[V] with Converter[V]

/**
 * A main parameter has a number of values specified by its `arity`.
 *
 * The values can be retrieved as a sequence by the `values` option.
 *
 * A main parameter is different from a list parameter by the absence of a name
 * in the command line. There is, however, an internal name
 * (`MainParameter.internalName`) that can be used in the command line to avoid
 * ambiguities.
 *
 * Main-parameter values can be given at the beginning or at the end of the
 * arguments sequence (or both mixed up, but that may be confusing). Beware,
 * that values at the end might be consumed by a variable-arity parameter in a
 * position right before.
 *
 * @author h2b
 */
case class MainParameter (description: String, arity: Int, required: Boolean = false,
    defaults: Option[Seq[String]] = None) extends Parameter[String] with MultipleValues[String] {

  val names = Set(MainParameter.internalName)

}

object MainParameter {

  val internalName: String = "--"

}
