/*
  Utilib - A Scala Library of Programming Utilities
  
  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util

/**
 * An enumeration type for providing different logging levels.
 *
 * The levels are prioritized in the order as they are declared.
 * As a clue, dummy priority numbers are given below to show the ordering,
 * but beware that these numbers are not part of the specification nor of the
 * implementation.
 *
 * Copyright (C) 2008, 2015 Hans-Hermann Bode
 *
 * @version 2.0
 * @author h2b
 */
object Level extends Enumeration {

  type Level = Value

  /**
   * Flag to indicate any level. Lowest priority.
   *
   * All events should be logged.
   */
  val All = Value

  /**
   * Flag to indicate tracing level. Dummy priority 1.
   *
   * A fine-grained debug message, typically capturing the flow through the application.
   */
  val Trace = Value

  /**
   * Flag to indicate debugging level. Dummy priority 2.
   *
   * A general debugging event.
   */
  val Debug = Value

  /**
   * Flag to indicate configuration level. Dummy priority 3.
   *
   * A message level for static configuration messages.
   */
  val Config = Value

  /**
   * Flag to indicate information level. Dummy priority 4.
   *
   * An event for informational purposes.
   */
  val Info = Value

  /**
   * Flag to indicate warning level. Dummy priority 5.
   *
   * An event that might possible lead to an error.
   */
  val Warn = Value

  /**
   * Flag to indicate error level. Dummy priority 6.
   *
   * An error in the application, possibly recoverable.
   */
  val Error = Value

  /**
   * Flag to indicate fatal level. Dummy priority 7.
   *
   * A severe error that will prevent the application from continuing.
   */
  val Fatal = Value

  /**
   * Flag to indicate no level. Highest priority.
   *
   * No events will be logged.
   */
  val Off = Value

}