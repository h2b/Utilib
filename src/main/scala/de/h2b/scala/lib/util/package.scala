/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib

import java.text.{ DateFormat, ParseException }
import java.util.Date

import de.h2b.scala.lib.phys.Time

/**
 * This is a Scala library of programming utilities. It is not intended to
 * comprise a systematic collection but contains some tools that may be
 * useful for some applications.
 *
 * Some highlights are a number of probability distributions, a command-line
 * interface, a configuration package, an RSS utility, a class to handle
 * temporary directories, control structures, a logger trait and a timer.
 */
package object util {

  class StringCompareIgnoreSet (val set: Set[Char])

  implicit final val Whitespace: StringCompareIgnoreSet = new StringCompareIgnoreSet(Set(' ', '\t', '\n'))

  /**
   * Provides string similarity-comparison ignoring implicitly specified characters.
   *
   * The implicit specification offered by this package object ignores
   * whitespace.
   *
   * @author h2b
   */
  implicit class StringCompare (str: String) (implicit val ignored: StringCompareIgnoreSet) {

    private val ignoring = (c: Char) => ignored.set.contains(c)

    /**
     * Compares strings ignoring implicitly specified characters.
     */
    def ~= (other: String): Boolean = other match {
      case that: String => that.filterNot(ignoring)==str.filterNot(ignoring)
      case _ => false
    }

  }

  object StringCompare {}

  /**
   * Provides implicit string conversions.
   *
   * @author h2b
   */
  implicit class StringConv (str: String) {

	  /**
	   * This string must be composed of a sequence of values each followed by a
	   * time unit: "d" for days, "h" for hours, "m" for minutes, "s" for seconds
	   * and "ms" for milliseconds. The general format is: "xdxhxmxsxms", where
	   * "x" denotes a number (a sequence of digits). Each number/unit part is
	   * optional. The unit identifiers are case sensitive (lower case is
	   * mandatory). No whitespace is allowed.
	   *
	   * @example "3d25m".toTime //3 days and 25 minutes
	   * @example "5m12s125ms".toTime //5 minutes, 12 seconds and 125 milliseconds
	   *
	   * @return a new time converted from this string
	   * @throws IllegalArgumentException if this string cannot be converted to `Time`
	   */
	  @throws(classOf[IllegalArgumentException])
    def toTime: Time = Time(str)

    /**
     * Parses text from the beginning of this string to produce a date.
     *
     * @note May not use the entire text of the given string.
     *
     * @param format the date format to be used for parsing
     * @return a new date converted from this string
     * @throws ParseException if this string cannot be converted to `Date`
     */
	  @throws(classOf[ParseException])
    def toDate (format: DateFormat): Date = format.parse(str)

  }
  
  /**
   * Provides implicit additional string operations.
   * @since 0.4.2
   * @author h2b
   */
  implicit class StringOps (str: String) {
  
    /**
     * @note To be used instead of 'lines` or `linesIterator` which either are
     * deprecated or may not compile. 
     * @return an iterator of all lines embedded in this string, excluding 
     * trailing line separator characters.
     */
    def linesWithoutSeparators: Iterator[String] = 
      str.linesWithSeparators.map(_.stripLineEnd)
    
  }

}
