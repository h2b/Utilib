/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util.cli

import scala.collection.{ mutable, immutable }
import scala.collection.mutable.StringBuilder
import scala.util.{ Failure, Success }

class CommandLine private (val parameters: Set[_ <: Parameter[_]]) {

  import CommandLine._

  private val namesToParams = mutable.Map.empty[String, Parameter[_]]

  for {
    p <- parameters
    n <- p.names
  } {
    if (namesToParams.contains(n)) throw new IllegalArgumentException(s"ambiguous name: $n")
	  namesToParams += n -> p
  }

  /**
   * Parses the command line for parameters.
   *
   * @param args the arguments to be parsed
   * @throws ParameterException if some argument does not obey to the
   * format specified by its parameter (regarding to arity or type)
   * @throws CommandLineException if something is wrong with the arguments as a
   * whole
   */
  def parse (args: String*): Unit = parse(args.toArray)

  /**
   * Parses the command line for parameters.
   *
   * @param args the arguments to be parsed
   * @throws ParameterException if some argument does not obey to the
   * format specified by its parameter (regarding to arity or type)
   * @throws CommandLineException if something is wrong with the arguments as a
   * whole
   */
  def parse (args: Array[String]): Unit = {
    val mainValsHead = args.takeWhile(! namesToParams.contains(_))
    val map = immutable.ListMap.empty[Parameter[_], Seq[String]] //must preserve insertion order
    val paramsToArgs = rawScan(map, args drop mainValsHead.length)
    if (hasHelpParameter(paramsToArgs)) { assignHelpValue(paramsToArgs); return }
    val adjusted = adjustedParamsToArgs(paramsToArgs, mainValsHead)
    checkConsistency(adjusted)
    assignValues(adjusted)
  }

  private def rawScan (map: Map[Parameter[_], Seq[String]], args: Array[String]): Map[Parameter[_], Seq[String]] =
    if (args.isEmpty) map
    else {
      val name = args(0)
      assert(namesToParams.contains(name))
      val values = args.drop(1).takeWhile(! namesToParams.contains(_)).toSeq
      rawScan(map + (namesToParams(name) -> values), args.drop(values.length+1))
    }

  private def hasHelpParameter (map: Map[Parameter[_], Seq[String]]): Boolean = {
    val option = map.keySet collectFirst { case _: HelpParameter => true }
    option.isDefined
  }

  private def assignHelpValue (map: Map[Parameter[_], Seq[String]]): Unit =
	  map.keySet collect { case h: HelpParameter => h._value = Some(true) }

  private def checkConsistency (map: Map[Parameter[_], Seq[String]]) = {
	  //check, if all required params are there
    val required = parameters filter { (p: Parameter[_]) => p.required } map { (p: Parameter[_]) => p }
    val present = map.keySet
    val missing = required diff present
    if (! missing.isEmpty) throw CommandLineException(Some(s"missing parameters required: ${missing.mkString}"))
  }

  private def adjustedParamsToArgs (map: Map[Parameter[_], Seq[String]], head: Array[String]):
      Map[Parameter[_], Seq[String]] = {
    //strip possible main-parameter values from last argument if present
    //and add main parameter to map if values are given
    val (adjustedMap, mainVals) = transferred(map, head)
    if (mainVals.isEmpty) adjustedMap
    else {
      if (! namesToParams.contains(MainParameter.internalName))
        throw CommandLineException(Some(s"illegal main parameter found: ${mainVals.mkString}"))
      adjustedMap + (namesToParams(MainParameter.internalName) -> mainVals.toSeq)
    }
  }

  private def transferred (map: Map[Parameter[_], Seq[String]], head: Array[String]):
      (Map[Parameter[_], Seq[String]], Array[String]) =
    if (! map.isEmpty) {
      val last = map.last
      val mainValsTrail = trailingMainCandidate(last)
      val mainVals = head ++ mainValsTrail
      val adjustedMap = map - last._1 + (last._1 -> last._2.dropRight(mainValsTrail.length))
      (adjustedMap, mainVals)
    } else (map, head)

  private def trailingMainCandidate (pv: (Parameter[_], Seq[String])) = {
    val p = pv._1
    val vals = pv._2
		if (p.arity >= 0) vals.takeRight(vals.length - p.arity)
		else Seq.empty
  }

  private def assignValues (map: Map[Parameter[_], Seq[String]]) =
    for (p <- map.keys) {
      val values = map(p)
      checkArity(p, values)
      p match {
        case f: FlagParameter => f._value = Some(true)
        case v: ValueParameter[_] => v._value = Some(convert(v, values).head)
        case l: ListParameter[_] => l._values = Some(convert(l, values))
        case m: MainParameter => m._values = Some(values)
        case h: HelpParameter => h._value = Some(true)
      }
    }

  private def checkArity (p: Parameter[_], v: Seq[String]) = {
    val passed = if (p.arity>=0) v.length == p.arity else v.length >= -p.arity
    if (!passed) throw ParameterException(p, Some(s"wrong number of arguments: ${v.length}"))
  }

  private def convert [V] (param: Parameter[V] with Converter[V], vals: Seq[String]) = {
		var result = Seq.empty[V]
		for (v <- vals) {
		  param.convert(v) match {
		    case Success(s) => result :+= s
		    case Failure(f) => throw ParameterException(param, Some("conversion error"), Some(f))
		  }
		}
		result
  }

  /**
   * A usage message of this command line in a two-column layout.
   *
   * The first column shows the parameter names in one line, the second column shows
   * one ore more lines of description. The lines of the description are taken
   * from the parameter descriptions as given, no automatic word wrap is done.
   *
   * @param commandName the name of this command
   * @param namesColWidth minimum width of the names column (defaults to 0)
   * @param extraInfo additional information to be append to the string
   * (unformatted, defaults to empty string)
   * @return the usage message
   */
  def usage (commandName: String, namesColWidth: Int = 0, extraInfo: String = ""): String = {
    val options = optionsColl
    val main = mainPar
    val builder = new StringBuilder(usagePrefix + eol + tab + commandName + " ")
    if (! options.isEmpty) builder ++= optionsPrefix
    if (main.isDefined) builder ++= main.get.description + eol
    else builder ++= eol.toString
    if (! options.isEmpty) {
      builder ++= where + optionsPrefix + are + eol
      val sortedOptions = options.toSeq sortWith {
    	  (x,y) => x.names.headOption.getOrElse("") < y.names.headOption.getOrElse("")
    	}
    	for (p <- sortedOptions) builder ++= tab + p.usage(namesColWidth)
    }
    builder ++= requiredHint + eol + extraInfo
    builder.result()
  }

  private def optionsColl = parameters collect {
    case p: FlagParameter => p
    case p: HelpParameter => p
    case p: ValueParameter[_] => p
    case p: ListParameter[_] => p
  }

  private def mainPar = parameters collectFirst {
    case p: MainParameter => p
  }

}

object CommandLine {

	private val eol = '\n'
	private val tab = "  "
	private val usagePrefix = "Usage:"
	private val optionsPrefix = "options "
	private val where = "where "
	private val are = "are:"
	private val requiredHint = "Required arguments are marked with *."

	/**
	 * @param parameters
	 * @return a new command line object
	 * @throws IllegalArgumentException if parameters have ambiguous names
	 */
	def apply (parameters: Set[_ <:Parameter[_]]) = new CommandLine(parameters)

}
