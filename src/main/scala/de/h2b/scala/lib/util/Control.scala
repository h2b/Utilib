/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util

import scala.annotation.tailrec
import scala.language.reflectiveCalls
import scala.util.{ Failure, Success, Try }

object Control {

	/**
   * Provides using a resource and ensures closing it even in case of an exception.
   *
   * From the book ''Beginning Scala'' by David Pollak.
   *
   * @see [[http://alvinalexander.com/scala/using-control-structure-beginning-scala-david-pollak]]
   *
	 * @param resource a closable resource of structural type `{def close(): Unit}`
	 * @param doIt maps the resource to a result
	 * @return the result
	 */
	def using [A <: {def close(): Unit}, B] (resource: A) (doIt: A => B): B = try {
		doIt(resource)
	} finally {
		resource.close()
	}

	/**
	 * Executes an operation once and -- when it fails with an exception -- retries
	 * it several times until no exception is thrown or the specified number of
	 * retries is exceeded.
	 *
	 * @note The maximum number of executions is equal to the maximum number of
	 * retries plus 1.
	 *
	 * @param n maximum number of retries
	 * @param delayMs milliseconds to wait between retries (non-negative value,
	 * defaults to 0)
	 * @param doIt operation to be executed
	 * @return the result of the last try of the operation in case of success
   * @throws IllegalArgumentException if the value of `delayMs` is negative
	 * @throws Exception whatever is thrown by the last try of the operation in case
	 * of no success
	 */
	@tailrec
	def retry [A] (n: Int, delayMs: Long = 0L) (doIt: => A): A = Try(doIt) match {
	  case Success(t) => t
	  case Failure(t) if (n>0) => Timer.delay(delayMs); retry(n-1)(doIt)
	  case Failure(t) => throw t
	}

}