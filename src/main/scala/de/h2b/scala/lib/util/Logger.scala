/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util

import java.util.Date

import scala.annotation.elidable

import de.h2b.scala.lib.util.Level.{ Config, Debug, Error, Fatal, Info, Level, Trace, Warn }

/**
 * An interface for logging events.
 *
 * Copyright (C) 2008, 2015 Hans-Hermann Bode
 *
 * @author h2b
 */
trait Logger {

	/**
	 * The current level used for logging messages.
	 *
	 * Defaults to {@code Level.Info}.
	 */
  private var level: Level = Info

  protected val name: String

  /**
   * Logs a tracing message.
   *
   * Elidable at level {@code elidable.FINER}.
   *
   * @param msg the message
   */
  @elidable(elidable.FINER)
  def trace (msg: => String): Unit = log(Trace, msg)

  /**
   * Logs a debugging message.
   *
   * Elidable at level {@code elidable.FINE}.
   *
   * @param msg the message
   */
  @elidable(elidable.FINE)
  def debug (msg: => String): Unit = log(Debug, msg)

  /**
   * Logs a configuration message.
   *
   * Elidable at level {@code elidable.CONFIG}.
   *
   * @param msg the message
   */
  @elidable(elidable.CONFIG)
  def config (msg: => String): Unit = log(Config, msg)

  /**
   * Logs an informational message.
   *
   * Elidable at level {@code elidable.INFO}.
   *
   * @param msg the message
   */
  @elidable(elidable.INFO)
  def info (msg: => String): Unit = log(Info, msg)

  /**
   * Logs a warning message.
   *
   * Elidable at level {@code elidable.WARNING}.
   *
   * @param msg the message
   */
  @elidable(elidable.WARNING)
  def warn (msg: => String): Unit = log(Warn, msg)

  /**
   * Logs an error message.
   *
   * Elidable at level {@code elidable.SEVERE}.
   *
   * @param msg the message
   */
  @elidable(elidable.SEVERE)
  def error (msg: => String): Unit = log(Error, msg)

  /**
   * Logs a fatal message.
   *
   * Elidable at level {@code elidable.SEVERE}.
   *
   * @param msg the message
   */
  @elidable(elidable.SEVERE)
  def fatal (msg: => String): Unit = log(Fatal, msg)

  /**
	 * Logs a message provided that the particular level specified is higher than
	 * or equal to the current level of this logger.
	 *
   * @note In contrast to the individual logging methods like {@code info},
   * {@code debug} etc., this method is not elidable.
   *
   * @param lev the level
   * @param msg the message
   */
  def log (lev: Level, msg: => String): Unit =
    if (isEnabled(lev)) log(s"+++ $lev ($name) " + new Date() + s": $msg")

	protected def log (msg: => String): Unit

  /**
   * States whether a level is enabled for this logger.
   *
   * The level specified is enabled if it is higher than or equal to the current
   * level of this logger.
   *
   * @param lev the level
   * @return    {@code true} if {@code lev} is enabled, {@code false} if not
   */
  def isEnabled (lev: Level): Boolean = lev >= level

  /**
   * Sets the current level of this logger.
   *
   * Since this logger itself is returned by this method, it can be used in a
   * declaration like in the example below.
   *
   * @example val logger = Logger("name") at Level.Debug
   *
   * @param lev the level
   * @return this logger
   */
  def at (lev: Level): Logger = {
	  level = lev
		this
  }

}

/**
 * Companion object that both serves as a global logger instance and provides
 * factory methods to create individual instances.
 *
 * @author h2b
 */
object Logger extends ConsoleLogger("global") {

  def apply (name: String): Logger = new ConsoleLogger(name)

  def apply (cl: Class[_]): Logger = apply(cl.getName)

}

/**
 * Logger implementation that writes logging messages to the console.
 *
 * @constructor
 *
 * @param id identifying name of this logger
 *
 * @author h2b
 */
class ConsoleLogger (id: String) extends Logger {

  protected val name: String = id

	protected def log (msg: => String): Unit = println(msg)

  override def toString: String = getClass.getName + "(" + name + ")"

}