/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util

/**
 * This package provides various configuration sources under a common trait
 * `Config`, which is basically an immutable map.
 * The following implementations are available:
 *
 * - `ArgsConfig` loads key/value pairs from an array of strings: array
 * elements that contain a separator char (defined by
 * `ArgsConfig.SeparatorChar`) are considered as key/value pairs, while
 * elements not consumed by this rule can be retrieved by the `remaining`
 * array (makes it suitable to scan a command line for such key/value pairs
 * and parsing the rest with tools like described in [U1 Command-Line
 * Interface](#u1-command-line-interface).
 *
 * - `FileConfig` loads key/value pairs from a specified resource file which
 * must conform to the `java.util.Properties#load(InputStream)` method; there
 * is also a factory method where the resource is looked up under the modified
 * package name of a specified class.
 *
 * - `BundleConfig` loads key/value pairs from a resource bundle (according to
 * the `java.util.ResourceBundle` class) of a specified base name using a
 * locale option; there is also a factory method where the resource bundle is
 * looked up under the modified package name of a specified class.
 *
 * * `SystemPropertiesConfig` is a `Config` object that represents the system
 * properties of the JVM.
 *
 * * `SystemEnvironmentConfig` is a `Config` object that represents the system
 * environment; note that the system may not support environment variables --
 * in this case, this object is empty.
 *
 * Multiple `Config`s can be combined using the standard map `++` operators,
 * yielding an overall `Config`.
 *
 * @example {{{
 *  import de.h2b.scala.lib.util.config._
 *  val argsConfig = ArgsConfig("-x key1=val1 /y key2=val2 abc".split(' '))
 *  val sysConfig = SystemPropertiesConfig
 *  val otherConfig = Config("one"->1, "two"->2, "three"->3)
 *  val config = argsConfig ++ sysConfig ++ otherConfig
 *  println(config.get("key1")) //> Some(val1)
 *  println(config.get("java.version")) //> Some(1.8.0_121)
 *  println(config.get("two")) //> Some(2)
 *  println(argsConfig.remaining.mkString(" ")) //> -x /y abc
 * }}}
 */
package object config
