/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util.config

import scala.collection.immutable.Map
import scala.collection.mutable.{ Map => MutableMap }
import scala.collection.mutable.ArrayBuilder

/**
 * A `Config` that loads key/value pairs from an args array.
 *
 * Only elements that contain a separator char (defined by
 * `ArgsConfig.SeparatorChar`) are considered as a key/value pair. The substring
 * before the first separator char is the key, everything after it is the value
 * (even if contains more separator chars).
 *
 * Elements not consumed by this rule can be retrieved by the `remaining` array.
 *
 * @note According to the rule, one or both parts of a key/value pair may be the
 * empty string.
 *
 * @constructor
 *
 * @param args the args array
 *
 * @author h2b
 */
class ArgsConfig private (private val args: Array[String]) extends Config[String, String] {

  private val (foundProps, remainingArgs) = scan()

  protected val props: Map[String, String] = foundProps

  /**
   * The remaining elements of `args` not consumed by this config.
   */
  val remaining: Array[String] = remainingArgs

  private def scan (): (Map[String, String], Array[String]) = {
    val mbuilder = MutableMap.empty[String, String]
    val abuilder = new ArrayBuilder.ofRef[String]
    for (arg <- args)
      if (isProp(arg, ArgsConfig.SeparatorChar)) mbuilder += prop(arg, ArgsConfig.SeparatorChar)
      else abuilder += arg
    (mbuilder.toMap, abuilder.result())
  }

  private def isProp (a: String, sep: Char) = a.contains(sep)

  private def prop (a: String, sep: Char) = {
    val s = a.indexOf(sep)
    (a.take(s), a.drop(s+1))
  }

}

object ArgsConfig {

	/**
	 * Key/value separator.
	 */
	final val SeparatorChar = '='

	/**
	 * Loads key/value pairs from an args array.
	 *
	 * Only elements that contain a separator char (defined by
	 * `ArgsConfig.SeparatorChar`) are considered as a key/value pair. The substring
	 * before the first separator char is the key, everything after it is the value
	 * (even if contains more separator chars).
	 *
	 * Elements not consumed by this rule can be retrieved by the `remaining` array.
	 *
	 * @note According to the rule, one or both parts of a key/value pair may be the
	 * empty string.
	 *
	 * @param args the args array
	 * @return a `Config` with key/value pairs loaded from an args array
	 */
  def apply(args: Array[String]): ArgsConfig = new ArgsConfig(args)

}
