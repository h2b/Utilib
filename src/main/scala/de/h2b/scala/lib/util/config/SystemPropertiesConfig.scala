/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util.config

import scala.collection.immutable.Map
import scala.sys.SystemProperties

/**
 * A `Config` that represents the system properties of the JVM.
 *
 * @note If a security manager exists and doesn't allow access to the system
 * properties, this config object is empty.
 *
 * @author h2b
 */
object SystemPropertiesConfig extends Config[String, String] {

  protected val props: Map[String, String] = new SystemProperties().toMap

}
