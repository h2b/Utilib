/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util

import scala.collection.immutable.{ Map => ImmutableMap }
import scala.collection.mutable.{ Map => MutableMap }

object PerformanceMeter {

	/**
	 * A task the performance of which shall be measured.
	 *
	 * @author h2b
	 */
	trait Task {
    /**Optional initialization actions that shall not be measured.*/
    def prePerform (): Unit = {}
    /**Actions that shall be measured.*/
    def perform (): Unit
    /**Optional finalization actions that shall not be measured.*/
    def postPerform (): Unit = {}
    override def toString = this.getClass.getSimpleName
  }

  /**
   * Returns the measurements of the total times of performing each task of a
   * sequence several times.
   *
   * @param tasks
   * @param repeats number of repetitions per task
   * @return a map that contains task/measuring pairs, measurements are in Nanoseconds
   */
  def measurements (tasks: Seq[Task], repeats: Int): ImmutableMap[Task, Long] = {
		  val result = MutableMap.empty[Task, Long]
		  for (task <- tasks) result(task) = totalTime(task, repeats)
		  result.toMap
  }

  /**
   * Measures the total time of performing one task several times.
   *
   * @param task
   * @param repeats number of repetitions
   * @return total time in Nanoseconds
   */
  def totalTime (task: Task, repeats: Int): Long = {
    var result = 0L
    for (i <- 1 to repeats) result += time(task)
    result
  }

	/**
	 * Measures the running time of performing one task once.
	 *
	 * @param task
	 * @return running time in Nanoseconds
	 */
	def time (task: Task): Long = {
	  task.prePerform()
		val timer = Timer.nanoTimer()
		task.perform()
		val t = timer.stop()
		task.postPerform()
		Logger.trace(s"${task}: time=${t}ns")
		t
  }

}