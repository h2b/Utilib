/*
  Utilib - A Scala Library of Programming Utilities

  Copyright 2015-2018, 2020 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.util

import de.h2b.scala.lib.phys.Time

/**
 * Provides timer functionality.
 * <p>
 * Based on my Java class Timer (as of 2006, has been nested in Clock class)
 * that itself had a long history of Modula2 (1987-90) and C (1994)
 * implementations.
 * <p/>
 * The timer will start immediately on construction, so it is not required to
 * start or reset it explictely (although it does no harm).
 * Arbitrary many start (implicit or explicit) and stop cycles can be passed
 * through. The times measured on each cycle are cumulated and
 * the result is returned by the reading method, while the lap method adds the
 * time since the last stop to that result.
 * <p/>
 * Copyright (C) 1987, 1989, 1990, 1994, 2006, 2015 Hans-Hermann Bode
 *
 * @version 3.0
 * @author h2b
 */
trait Timer {

  private var startMs = 0L
  private var stopMs = 0L

  reset()

  /**
   * Sets the timer to zero.
   */
  def reset () = {
	  startMs = systemTime
		stopMs = startMs
  }

  protected def systemTime: Long

  /**
   * Starts a new measuring cycle without resetting the timer.
   */
  def start () = {
    val t = systemTime
    startMs = t-reading
    stopMs = t
  }

  /**
   * Stops the current measuring cycle without resetting the timer.
   *
   * @return the reading (cumulative time of all cycles so far)
   */
  def stop () = {
    stopMs = systemTime
    reading
  }

  /**
   * @return the current timer value (like the ''lap'' function of a stopwatch)
   */
  def lap = systemTime-startMs

  /**
   * @return the cumulative time of all start/stop cycles so far
   */
  def reading = stopMs-startMs

}

object Timer {

  /**
   * @return a timer with default precision (milliseconds)
   */
  def apply (): Timer = new MsTimer

  /**
   * @return a timer with milliseconds precision
   */
  def milliTimer (): Timer = new MsTimer

  /**
   * @return a timer with nanoseconds precision
   */
  def nanoTimer (): Timer = new NsTimer

  /**
   * Waits for the given amount of time.
   *
   * @param ms milliseconds (non-negative value)
   * @throws IllegalArgumentException if the value of ms is negative
   */
  def delay (ms: Long) = Thread.sleep(ms)

}

class MsTimer private[util] extends Timer {

  protected def systemTime = System.nanoTime / 1000000

  override def toString () = Time(lap).toString

}

class NsTimer private[util] extends Timer {

  protected def systemTime = System.nanoTime

  override def toString () = Time(lap / 1000000).toString

}