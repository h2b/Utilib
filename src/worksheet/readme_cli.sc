object readme_cli {

  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  import de.h2b.scala.lib.util.cli._
  import de.h2b.scala.lib.util.cli.Converter._
    
	val overwrite = FlagParameter(Set("-o", "--overwrite"), "overwrite target")
                                                  //> overwrite  : de.h2b.scala.lib.util.cli.FlagParameter = FlagParameter(Set(-o,
                                                  //|  --overwrite),overwrite target)
	val mode = ValueParameter(Set("-m"), "mode", default=Some(0))
                                                  //> mode  : de.h2b.scala.lib.util.cli.ValueParameter[Int] = ValueParameter(Set(-
                                                  //| m),mode,false,Some(0))
	val main = MainParameter("source target", arity=2)
                                                  //> main  : de.h2b.scala.lib.util.cli.MainParameter = MainParameter(source targe
                                                  //| t,2,false,None)
	CommandLine(Set(overwrite, mode, main)).parse("-o -m 1 from to".split(' '))
	println(overwrite.value)                  //> Some(true)
	println(mode.value)                       //> Some(1)
	println(main.values)                      //> Some(WrappedArray(from, to))

}