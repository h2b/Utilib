object readme_config {

  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
	import de.h2b.scala.lib.util.config._
	
	val argsConfig = ArgsConfig("-x key1=val1 /y key2=val2 abc".split(' '))
                                                  //> argsConfig  : de.h2b.scala.lib.util.config.ArgsConfig = Map(key1 -> val1, ke
                                                  //| y2 -> val2)
	val sysConfig = SystemPropertiesConfig    //> sysConfig  : de.h2b.scala.lib.util.config.SystemPropertiesConfig.type = Map(
                                                  //| java.io.tmpdir -> /tmp, line.separator -> 
                                                  //| , path.separator -> :, sun.management.compiler -> HotSpot 64-Bit Tiered Comp
                                                  //| ilers, sun.cpu.endian -> little, java.specification.version -> 1.8, java.vm.
                                                  //| specification.name -> Java Virtual Machine Specification, java.vendor -> Ora
                                                  //| cle Corporation, java.vm.specification.version -> 1.8, user.home -> /home/h2
                                                  //| b, file.encoding.pkg -> sun.io, sun.arch.data.model -> 64, sun.boot.library.
                                                  //| path -> /usr/lib64/jvm/java-1.8.0-openjdk-1.8.0/jre/lib/amd64, user.dir -> /
                                                  //| home/h2b, java.library.path -> /usr/java/packages/lib/amd64:/usr/lib64:/lib6
                                                  //| 4:/lib:/usr/lib, sun.cpu.isalist -> , os.arch -> amd64, java.vm.version -> 2
                                                  //| 5.121-b13, java.endorsed.dirs -> /usr/lib64/jvm/java-1.8.0-openjdk-1.8.0/jre
                                                  //| /lib/endorsed, java.runtime.version -> 1.8.0_121-b13, java.vm.info -> mixed 
                                                  //| mode, java.ext.dirs -> /usr/lib64/jvm/java-1.8.0-openjdk-1
                                                  //| Output exceeds cutoff limit.
	val otherConfig = Config("one"->1, "two"->2, "three"->3)
                                                  //> otherConfig  : de.h2b.scala.lib.util.config.Config[String,Int] = Map(one -> 
                                                  //| 1, three -> 3, two -> 2)
	val config = argsConfig ++ sysConfig ++ otherConfig
                                                  //> config  : scala.collection.immutable.Map[String,Any] = Map(java.io.tmpdir ->
                                                  //|  /tmp, line.separator -> 
                                                  //| , path.separator -> :, sun.management.compiler -> HotSpot 64-Bit Tiered Comp
                                                  //| ilers, sun.cpu.endian -> little, java.specification.version -> 1.8, java.vm.
                                                  //| specification.name -> Java Virtual Machine Specification, three -> 3, java.v
                                                  //| endor -> Oracle Corporation, java.vm.specification.version -> 1.8, user.home
                                                  //|  -> /home/h2b, file.encoding.pkg -> sun.io, sun.arch.data.model -> 64, sun.b
                                                  //| oot.library.path -> /usr/lib64/jvm/java-1.8.0-openjdk-1.8.0/jre/lib/amd64, u
                                                  //| ser.dir -> /home/h2b, java.library.path -> /usr/java/packages/lib/amd64:/usr
                                                  //| /lib64:/lib64:/lib:/usr/lib, sun.cpu.isalist -> , os.arch -> amd64, java.vm.
                                                  //| version -> 25.121-b13, two -> 2, java.endorsed.dirs -> /usr/lib64/jvm/java-1
                                                  //| .8.0-openjdk-1.8.0/jre/lib/endorsed, java.runtime.version -> 1.8.0_121-b13, 
                                                  //| java.vm.info -> mixed mode, java.ext.dirs -> /usr/lib64/jvm/java-1.8.0-open
                                                  //| Output exceeds cutoff limit.
	println(config.get("key1"))               //> Some(val1)
	println(config.get("java.version"))       //> Some(1.8.0_121)
	println(config.get("two"))                //> Some(2)
	println(argsConfig.remaining.mkString(" "))
                                                  //> -x /y abc
	
}