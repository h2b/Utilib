ThisBuild / organization := "de.h2b.scala.lib"

ThisBuild / organizationName := "private"

ThisBuild / organizationHomepage := Some(url("http://h2b.de"))

ThisBuild / homepage := Some(url("http://h2b.de"))

ThisBuild / startYear := Some(2015)

ThisBuild / description := """This is a Scala library of programming utilities.

It is not intended to comprise a systematic collection but contains some tools
that happen to be useful for myself and may be of interest for others.
"""

licenses := Seq("Apache License, Version 2.0" -> url("https://www.apache.org/licenses/LICENSE-2.0"))

pomExtra := Seq(
	<scm>
		<url>scm:git:https://gitlab.com/h2b/Utilib.git</url>
	</scm>,
	<developers>
		<developer>
			<id>h2b</id>
			<name>Hans-Hermann Bode</name>
			<email>projekte@h2b.de</email>
			<url>http://h2b.de</url>
			<roles>
				<role>Owner</role>
				<role>Architect</role>
				<role>Developer</role>
			</roles>
			<timezone>Europe/Berlin</timezone>
		</developer>
	</developers>
)
