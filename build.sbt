name := "Utilib"

lazy val scala213 = "2.13.1"
lazy val scala212 = "2.12.10"
lazy val scala211 = "2.11.12"
lazy val supportedScalaVersions = List(scala213, scala212, scala211)
crossScalaVersions := supportedScalaVersions

ThisBuild / scalaVersion := scala213
ThisBuild / scalacOptions ++= Seq("-deprecation", "-unchecked")

//Adds a `src/main/scala-2.13+` source directory for Scala 2.13 and newer
//and a `src/main/scala-2.12-` source directory for Scala versions older than 2.13
unmanagedSourceDirectories in Compile += {
  val sourceDir = (sourceDirectory in Compile).value
  CrossVersion.partialVersion(scalaVersion.value) match {
    case Some((2, n)) if n >= 13 => sourceDir / "scala-2.13+"
    case _                       => sourceDir / "scala-2.12-"
  }
}

libraryDependencies ++= Seq(
	"org.scala-lang.modules" %% "scala-xml" % "1.2.0",
	"org.scalatest" %% "scalatest" % "3.0.8" % "test",
	"junit" % "junit" % "4.13" % "test"
)
